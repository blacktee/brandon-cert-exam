using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_DELEGATE_NAMES
{
    public class UserModuleClass_DELEGATE_NAMES : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.BufferInput DEVICE_RX;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DELEGATE_NAME;
        private void FGET_NAME (  SplusExecutionContext __context__, CrestronString DATA ) 
            { 
            ushort NSEAT = 0;
            ushort NOFFSET = 0;
            ushort NSTART = 0;
            ushort NLENGTH = 0;
            
            CrestronString SNAME;
            SNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 135;
            NOFFSET = (ushort) ( 64 ) ; 
            __context__.SourceCodeLine = 136;
            NSEAT = (ushort) ( (Byte( DATA , (int)( (Functions.Length( "report delegatename" ) + 1) ) ) - NOFFSET) ) ; 
            __context__.SourceCodeLine = 137;
            NSTART = (ushort) ( (Functions.Length( "report delegatename" ) + 2) ) ; 
            __context__.SourceCodeLine = 138;
            NLENGTH = (ushort) ( ((Functions.Length( DATA ) - Functions.Length( "report delegatename" )) - 2) ) ; 
            __context__.SourceCodeLine = 139;
            SNAME  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( NSTART ) ,  (int) ( NLENGTH ) )  ) ; 
            __context__.SourceCodeLine = 140;
            if ( Functions.TestForTrue  ( ( 1)  ) ) 
                {
                __context__.SourceCodeLine = 140;
                Trace( "D_Name: fGet_Name results: Seat{0:d}, Start {1:d}, Len {2:d}, Name {3}\r\n", (short)NSEAT, (short)NSTART, (short)NLENGTH, SNAME ) ; 
                }
            
            __context__.SourceCodeLine = 141;
            DELEGATE_NAME [ NSEAT]  .UpdateValue ( SNAME  ) ; 
            
            }
            
        object DEVICE_RX_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                CrestronString STEMP;
                STEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 500, this );
                
                
                __context__.SourceCodeLine = 189;
                while ( Functions.TestForTrue  ( ( Functions.Find( "\u00FE" , DEVICE_RX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 191;
                    STEMP  .UpdateValue ( Functions.Gather ( "\u00FE" , DEVICE_RX )  ) ; 
                    __context__.SourceCodeLine = 192;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "report delegatename" , STEMP ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 194;
                        FGET_NAME (  __context__ , STEMP) ; 
                        __context__.SourceCodeLine = 195;
                        if ( Functions.TestForTrue  ( ( 1)  ) ) 
                            {
                            __context__.SourceCodeLine = 195;
                            Trace( "D_Name: Found name in {0}\r\n", STEMP ) ; 
                            }
                        
                        } 
                    
                    __context__.SourceCodeLine = 189;
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        DELEGATE_NAME = new InOutArray<StringOutput>( 24, this );
        for( uint i = 0; i < 24; i++ )
        {
            DELEGATE_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DELEGATE_NAME__AnalogSerialOutput__ + i, this );
            m_StringOutputList.Add( DELEGATE_NAME__AnalogSerialOutput__ + i, DELEGATE_NAME[i+1] );
        }
        
        DEVICE_RX = new Crestron.Logos.SplusObjects.BufferInput( DEVICE_RX__AnalogSerialInput__, 1024, this );
        m_StringInputList.Add( DEVICE_RX__AnalogSerialInput__, DEVICE_RX );
        
        
        DEVICE_RX.OnSerialChange.Add( new InputChangeHandlerWrapper( DEVICE_RX_OnChange_0, true ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_DELEGATE_NAMES ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint DEVICE_RX__AnalogSerialInput__ = 0;
    const uint DELEGATE_NAME__AnalogSerialOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
