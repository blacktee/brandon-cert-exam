#ifndef __S2_PASSWORD_S__BL_H__
#define __S2_PASSWORD_S__BL_H__




/*
* Constructor and Destructor
*/

/*
* DIGITAL_INPUT
*/
#define __S2_Password_S__BL_KEYPAD_ENTER_DIG_INPUT 0
#define __S2_Password_S__BL_KEYPAD_CLEAR_DIG_INPUT 1

#define __S2_Password_S__BL_KEYPAD_DIG_INPUT 2
#define __S2_Password_S__BL_KEYPAD_ARRAY_LENGTH 10

/*
* ANALOG_INPUT
*/

#define __S2_Password_S__BL_PASSWORD_IN_STRING_INPUT 0
#define __S2_Password_S__BL_PASSWORD_IN_STRING_MAX_LEN 50
CREATE_STRING_STRUCT( S2_Password_S__BL, __PASSWORD_IN, __S2_Password_S__BL_PASSWORD_IN_STRING_MAX_LEN );



/*
* DIGITAL_OUTPUT
*/
#define __S2_Password_S__BL_PASSWORD_IS_CORRECT_DIG_OUTPUT 0
#define __S2_Password_S__BL_PASSWORD_IS_INCORRECT_DIG_OUTPUT 1


/*
* ANALOG_OUTPUT
*/

#define __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT 0


/*
* Direct Socket Variables
*/




/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* STRING_PARAMETER
*/


/*
* INTEGER
*/


/*
* LONG_INTEGER
*/


/*
* SIGNED_INTEGER
*/


/*
* SIGNED_LONG_INTEGER
*/


/*
* STRING
*/
#define __S2_Password_S__BL_ENTERED_STRING_MAX_LEN 50
CREATE_STRING_STRUCT( S2_Password_S__BL, __ENTERED, __S2_Password_S__BL_ENTERED_STRING_MAX_LEN );
#define __S2_Password_S__BL_TEMP_STRING_MAX_LEN 50
CREATE_STRING_STRUCT( S2_Password_S__BL, __TEMP, __S2_Password_S__BL_TEMP_STRING_MAX_LEN );

/*
* STRUCTURE
*/

START_GLOBAL_VAR_STRUCT( S2_Password_S__BL )
{
   void* InstancePtr;
   struct GenericOutputString_s sGenericOutStr;
   unsigned short LastModifiedArrayIndex;

   DECLARE_IO_ARRAY( __KEYPAD );
   unsigned short __I;
   DECLARE_STRING_STRUCT( S2_Password_S__BL, __ENTERED );
   DECLARE_STRING_STRUCT( S2_Password_S__BL, __TEMP );
   DECLARE_STRING_STRUCT( S2_Password_S__BL, __PASSWORD_IN );
};

START_NVRAM_VAR_STRUCT( S2_Password_S__BL )
{
};



#endif //__S2_PASSWORD_S__BL_H__

