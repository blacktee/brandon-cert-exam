#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_Password_S__BL.h"

FUNCTION_MAIN( S2_Password_S__BL );
EVENT_HANDLER( S2_Password_S__BL );
DEFINE_ENTRYPOINT( S2_Password_S__BL );


DEFINE_INDEPENDENT_EVENTHANDLER( S2_Password_S__BL, 00000 /*Keypad_Clear*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_Password_S__BL ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 50 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
    
    ; 
    
    S2_Password_S__BL_Exit__Event_0:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_Password_S__BL, 00001 /*Keypad*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "1" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "2" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( "3" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "4" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_4__, sizeof( "5" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_4__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_5__, sizeof( "6" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_5__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_6__, sizeof( "7" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_6__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_7__, sizeof( "8" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_7__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_8__, sizeof( "9" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_8__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_9__, sizeof( "0" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_9__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_10__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_10__ );
    
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_11__, sizeof( "*" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_11__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_Password_S__BL ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "1" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "2" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, "3" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "4" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_4__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__, "5" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_5__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__, "6" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_6__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__, "7" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_7__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__, "8" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_8__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__, "9" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_9__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__, "0" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_10__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_11__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__, "*" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 55 );
    Globals->S2_Password_S__BL.__I = GetLocalLastModifiedArrayIndex ( S2_Password_S__BL ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 56 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   )  ; 
    SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 57 );
    
        {
        int __SPLS_TMPVAR__SWTCH_1__ = ( Globals->S2_Password_S__BL.__I) ;
        
            { 
            if ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 60 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 61 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 62 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 63 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 64 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 65 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 66 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 67 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 9) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 68 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 10) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 69 );
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                }
            
            } 
            
        }
        
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 72 );
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __TEMP  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 73 );
    __FN_FOREND_VAL__1 = (Len( GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )  ) - 1); 
    __FN_FORINIT_VAL__1 = 1; 
    for( Globals->S2_Password_S__BL.__I = 0; (__FN_FORINIT_VAL__1 > 0)  ? ((short)Globals->S2_Password_S__BL.__I  <= __FN_FOREND_VAL__1 ) : ((short)Globals->S2_Password_S__BL.__I  >= __FN_FOREND_VAL__1) ; Globals->S2_Password_S__BL.__I  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 75 );
        FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ )   ,  GLOBAL_STRING_STRUCT( S2_Password_S__BL, __TEMP  )   )  ; 
        FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __TEMP  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 73 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 77 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __TEMP  )   )  ; 
    SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
    
    ; 
    
    S2_Password_S__BL_Exit__Event_1:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_Password_S__BL, 00002 /*Keypad_Enter*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "Incorrect" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_Password_S__BL ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "Incorrect" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_Password_S__BL, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 84 );
    if ( (CompareStrings( GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __PASSWORD_IN  ) , 1 ) == 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 86 );
        Pulse ( INSTANCE_PTR( S2_Password_S__BL ) , 10, __S2_Password_S__BL_PASSWORD_IS_CORRECT_DIG_OUTPUT ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 87 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
        
        ; 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 92 );
        Pulse ( INSTANCE_PTR( S2_Password_S__BL ) , 10, __S2_Password_S__BL_PASSWORD_IS_INCORRECT_DIG_OUTPUT ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 93 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 94 );
        Delay ( 100) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 95 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GENERIC_STRING_OUTPUT( S2_Password_S__BL )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SetSerial( INSTANCE_PTR( S2_Password_S__BL ), __S2_Password_S__BL_PASSWORD_OUT_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_Password_S__BL ) );
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_Password_S__BL ) ); }
        
        ; 
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_Password_S__BL ), 97 );
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_Password_S__BL ) , GLOBAL_STRING_STRUCT( S2_Password_S__BL, __ENTERED  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    
    S2_Password_S__BL_Exit__Event_2:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_Password_S__BL_KEYPAD_ENTER_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_Password_S__BL, 00002 /*Keypad_Enter*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
                
            }
            break;
            
        case __S2_Password_S__BL_KEYPAD_CLEAR_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_Password_S__BL, 00000 /*Keypad_Clear*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
                
            }
            break;
            
        case __S2_Password_S__BL_KEYPAD_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_Password_S__BL, 00001 /*Keypad*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_Password_S__BL ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_Password_S__BL ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_Password_S__BL ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_Password_S__BL )
********************************************************************************/
EVENT_HANDLER( S2_Password_S__BL )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_Password_S__BL, __KEYPAD ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_Password_S__BL )
********************************************************************************/
FUNCTION_MAIN( S2_Password_S__BL )
{
    SAVE_GLOBAL_POINTERS ;
    
    SET_INSTANCE_POINTER ( S2_Password_S__BL );
    INITIALIZE_IO_ARRAY ( S2_Password_S__BL, __KEYPAD, IO_TYPE_DIGITAL_INPUT, __S2_Password_S__BL_KEYPAD_DIG_INPUT, __S2_Password_S__BL_KEYPAD_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_Password_S__BL, __PASSWORD_IN, e_INPUT_TYPE_STRING, __S2_Password_S__BL_PASSWORD_IN_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_Password_S__BL, __PASSWORD_IN, __S2_Password_S__BL_PASSWORD_IN_STRING_INPUT );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_Password_S__BL, __ENTERED, e_INPUT_TYPE_NONE, __S2_Password_S__BL_ENTERED_STRING_MAX_LEN );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_Password_S__BL, __TEMP, e_INPUT_TYPE_NONE, __S2_Password_S__BL_TEMP_STRING_MAX_LEN );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_Password_S__BL, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    
    
    S2_Password_S__BL_Exit__MAIN:
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
