using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_READFILE
{
    public class UserModuleClass_READFILE : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput READ;
        Crestron.Logos.SplusObjects.DigitalInput WRITE;
        Crestron.Logos.SplusObjects.StringInput FILEPATHIN__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput TAGIN__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SERIALIN;
        Crestron.Logos.SplusObjects.StringOutput STATUS__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SERIALOUT;
        ushort I = 0;
        short NFILEHANDLE = 0;
        short IERRORCODE = 0;
        uint SIZE = 0;
        CrestronString [] SERIALSAVE;
        CrestronString FILEEXTENSION__DOLLAR__;
        CrestronString FILEPATH__DOLLAR__;
        CrestronString TAG__DOLLAR__;
        object SERIALIN_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 165;
                SERIALSAVE [ Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ]  .UpdateValue ( SERIALIN [ Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ]  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object FILEPATHIN__DOLLAR___OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 169;
            FILEPATH__DOLLAR__  .UpdateValue ( FILEPATHIN__DOLLAR__  ) ; 
            __context__.SourceCodeLine = 170;
            if ( Functions.TestForTrue  ( ( Functions.Length( TAG__DOLLAR__ ))  ) ) 
                { 
                __context__.SourceCodeLine = 174;
                FILEPATH__DOLLAR__  .UpdateValue ( FILEPATH__DOLLAR__ + TAG__DOLLAR__ + ".dat"  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 178;
                FILEPATH__DOLLAR__  .UpdateValue ( FILEPATH__DOLLAR__ + ".dat"  ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object TAGIN__DOLLAR___OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 183;
        TAG__DOLLAR__  .UpdateValue ( TAGIN__DOLLAR__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object WRITE_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 188;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)50; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 190;
            SERIALSAVE [ I ]  .UpdateValue ( SERIALIN [ I ]  ) ; 
            __context__.SourceCodeLine = 188;
            } 
        
        __context__.SourceCodeLine = 192;
        if ( Functions.TestForTrue  ( ( Functions.Length( FILEPATH__DOLLAR__ ))  ) ) 
            { 
            __context__.SourceCodeLine = 194;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 195;
            NFILEHANDLE = (short) ( FileOpenShared( FILEPATH__DOLLAR__ ,(ushort) ((1 | 256) | 32768) ) ) ; 
            __context__.SourceCodeLine = 196;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NFILEHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 198;
                IERRORCODE = (short) ( WriteStringArray( (short)( NFILEHANDLE ) , SERIALSAVE ) ) ; 
                __context__.SourceCodeLine = 199;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( IERRORCODE > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 199;
                    STATUS__DOLLAR__  .UpdateValue ( "Serials Written"  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 200;
                    STATUS__DOLLAR__  .UpdateValue ( "Serial Save Error"  ) ; 
                    } 
                
                } 
            
            __context__.SourceCodeLine = 204;
            FileClose (  (short) ( NFILEHANDLE ) ) ; 
            __context__.SourceCodeLine = 205;
            EndFileOperations ( ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 209;
            STATUS__DOLLAR__  .UpdateValue ( "No File Path"  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object READ_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 215;
        StartFileOperations ( ) ; 
        __context__.SourceCodeLine = 217;
        NFILEHANDLE = (short) ( FileOpenShared( FILEPATH__DOLLAR__ ,(ushort) (0 | 32768) ) ) ; 
        __context__.SourceCodeLine = 218;
        SIZE = (uint) ( FileLength( (short)( NFILEHANDLE ) ) ) ; 
        __context__.SourceCodeLine = 219;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NFILEHANDLE >= 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 221;
            ReadStringArray (  (short) ( NFILEHANDLE ) ,  ref SERIALSAVE ) ; 
            } 
        
        __context__.SourceCodeLine = 223;
        FileClose (  (short) ( NFILEHANDLE ) ) ; 
        __context__.SourceCodeLine = 224;
        EndFileOperations ( ) ; 
        __context__.SourceCodeLine = 227;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)50; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 229;
            SERIALOUT [ I]  .UpdateValue ( SERIALSAVE [ I ]  ) ; 
            __context__.SourceCodeLine = 227;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    FILEEXTENSION__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8, this );
    FILEPATH__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    TAG__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    SERIALSAVE  = new CrestronString[ 51 ];
    for( uint i = 0; i < 51; i++ )
        SERIALSAVE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    READ = new Crestron.Logos.SplusObjects.DigitalInput( READ__DigitalInput__, this );
    m_DigitalInputList.Add( READ__DigitalInput__, READ );
    
    WRITE = new Crestron.Logos.SplusObjects.DigitalInput( WRITE__DigitalInput__, this );
    m_DigitalInputList.Add( WRITE__DigitalInput__, WRITE );
    
    FILEPATHIN__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( FILEPATHIN__DOLLAR____AnalogSerialInput__, 50, this );
    m_StringInputList.Add( FILEPATHIN__DOLLAR____AnalogSerialInput__, FILEPATHIN__DOLLAR__ );
    
    TAGIN__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( TAGIN__DOLLAR____AnalogSerialInput__, 12, this );
    m_StringInputList.Add( TAGIN__DOLLAR____AnalogSerialInput__, TAGIN__DOLLAR__ );
    
    SERIALIN = new InOutArray<StringInput>( 50, this );
    for( uint i = 0; i < 50; i++ )
    {
        SERIALIN[i+1] = new Crestron.Logos.SplusObjects.StringInput( SERIALIN__AnalogSerialInput__ + i, SERIALIN__AnalogSerialInput__, 100, this );
        m_StringInputList.Add( SERIALIN__AnalogSerialInput__ + i, SERIALIN[i+1] );
    }
    
    STATUS__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( STATUS__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( STATUS__DOLLAR____AnalogSerialOutput__, STATUS__DOLLAR__ );
    
    SERIALOUT = new InOutArray<StringOutput>( 50, this );
    for( uint i = 0; i < 50; i++ )
    {
        SERIALOUT[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SERIALOUT__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SERIALOUT__AnalogSerialOutput__ + i, SERIALOUT[i+1] );
    }
    
    
    for( uint i = 0; i < 50; i++ )
        SERIALIN[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( SERIALIN_OnChange_0, false ) );
        
    FILEPATHIN__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( FILEPATHIN__DOLLAR___OnChange_1, false ) );
    TAGIN__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( TAGIN__DOLLAR___OnChange_2, false ) );
    WRITE.OnDigitalPush.Add( new InputChangeHandlerWrapper( WRITE_OnPush_3, false ) );
    READ.OnDigitalPush.Add( new InputChangeHandlerWrapper( READ_OnPush_4, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_READFILE ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint READ__DigitalInput__ = 0;
const uint WRITE__DigitalInput__ = 1;
const uint FILEPATHIN__DOLLAR____AnalogSerialInput__ = 0;
const uint TAGIN__DOLLAR____AnalogSerialInput__ = 1;
const uint SERIALIN__AnalogSerialInput__ = 2;
const uint STATUS__DOLLAR____AnalogSerialOutput__ = 0;
const uint SERIALOUT__AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
