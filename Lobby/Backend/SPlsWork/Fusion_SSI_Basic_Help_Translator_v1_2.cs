using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace CrestronModule_FUSION_SSI_BASIC_HELP_TRANSLATOR_V1_2
{
    public class CrestronModuleClass_FUSION_SSI_BASIC_HELP_TRANSLATOR_V1_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput SEND_MESSAGE;
        Crestron.Logos.SplusObjects.DigitalInput CANCEL_MESSAGE;
        Crestron.Logos.SplusObjects.StringInput UNFORMATTED_MESSAGE;
        Crestron.Logos.SplusObjects.BufferInput FUSION_HELP_RESPONSE;
        Crestron.Logos.SplusObjects.DigitalOutput MESSAGE_RECIEVED;
        Crestron.Logos.SplusObjects.DigitalOutput REQUEST_RESOLVED;
        Crestron.Logos.SplusObjects.StringOutput OPEN_REQUESTS;
        Crestron.Logos.SplusObjects.StringOutput UNFORMATTED_RESPONSE;
        Crestron.Logos.SplusObjects.StringOutput FUSION_HELP_REQUEST;
        Crestron.Logos.SplusObjects.StringOutput FORMATTED_RESPONSE;
        UShortParameter DATE_FORMAT;
        UShortParameter TIME_FORMAT;
        CrestronString UNFORMAT_MSG_TEMP;
        ushort OPENIDS = 0;
        private CrestronString MAKE_THE_TIME (  SplusExecutionContext __context__ ) 
            { 
            ushort HR = 0;
            
            CrestronString TIME_TEMP;
            TIME_TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            
            CrestronString TIME_STAMP;
            TIME_STAMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, this );
            
            
            __context__.SourceCodeLine = 110;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TIME_FORMAT  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 112;
                return ( Functions.Time ( ) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 114;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TIME_FORMAT  .Value == 2))  ) ) 
                    { 
                    __context__.SourceCodeLine = 116;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ((Functions.GetHourNum() / 12) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 118;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.GetHourNum() == 12))  ) ) 
                            { 
                            __context__.SourceCodeLine = 120;
                            HR = (ushort) ( Functions.GetHourNum() ) ; 
                            __context__.SourceCodeLine = 121;
                            TIME_STAMP  .UpdateValue ( "PM"  ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 125;
                            HR = (ushort) ( (Functions.GetHourNum() - 12) ) ; 
                            __context__.SourceCodeLine = 126;
                            TIME_STAMP  .UpdateValue ( "PM"  ) ; 
                            } 
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 131;
                        HR = (ushort) ( Functions.GetHourNum() ) ; 
                        __context__.SourceCodeLine = 132;
                        TIME_STAMP  .UpdateValue ( "AM"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 136;
                    MakeString ( TIME_TEMP , "{0:d2}:{1:d2}:{2:d2} {3}", (ushort)HR, (ushort)Functions.GetMinutesNum(), (ushort)Functions.GetSecondsNum(), TIME_STAMP ) ; 
                    __context__.SourceCodeLine = 137;
                    return ( TIME_TEMP ) ; 
                    } 
                
                }
            
            
            return ""; // default return value (none specified in module)
            }
            
        private CrestronString MAKE_THE_ID (  SplusExecutionContext __context__ ) 
            { 
            CrestronString TEMP;
            TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            
            __context__.SourceCodeLine = 144;
            MakeString ( TEMP , "{0:d}{1:d}{2:d}00{3:d3}", (ushort)Functions.GetMinutesNum(), (ushort)Functions.GetDateNum(), (ushort)Functions.GetHourNum(), (ushort)Functions.Random( (ushort)( 1 ) , (ushort)( 256 ) )) ; 
            __context__.SourceCodeLine = 146;
            return ( TEMP ) ; 
            
            }
            
        private CrestronString FORMAT_THE_RESPONSE (  SplusExecutionContext __context__, CrestronString DATA ) 
            { 
            ushort LOC_A = 0;
            ushort LOC_B = 0;
            ushort I = 0;
            
            CrestronString ADMIN;
            CrestronString MSG_TEMP;
            CrestronString FMT_MSG;
            CrestronString CURRENTID;
            CrestronString TIME_TEMP;
            ADMIN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            MSG_TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            FMT_MSG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
            CURRENTID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            TIME_TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            
            
            __context__.SourceCodeLine = 156;
            LOC_A = (ushort) ( Functions.Find( "<ID>" , DATA ) ) ; 
            __context__.SourceCodeLine = 157;
            if ( Functions.TestForTrue  ( ( LOC_A)  ) ) 
                { 
                __context__.SourceCodeLine = 159;
                TIME_TEMP  .UpdateValue ( MAKE_THE_TIME (  __context__  )  ) ; 
                __context__.SourceCodeLine = 160;
                LOC_A = (ushort) ( (LOC_A + 4) ) ; 
                __context__.SourceCodeLine = 161;
                LOC_B = (ushort) ( Functions.Find( "</" , DATA , LOC_A ) ) ; 
                __context__.SourceCodeLine = 162;
                CURRENTID  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( LOC_A ) ,  (int) ( (LOC_B - LOC_A) ) )  ) ; 
                __context__.SourceCodeLine = 164;
                if ( Functions.TestForTrue  ( ( Functions.Find( "new_user" , DATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 165;
                    ; 
                    __context__.SourceCodeLine = 167;
                    LOC_A = (ushort) ( (Functions.Find( "<Message>" , DATA ) + 9) ) ; 
                    __context__.SourceCodeLine = 168;
                    LOC_B = (ushort) ( Functions.Find( "</" , DATA , LOC_A ) ) ; 
                    __context__.SourceCodeLine = 170;
                    MSG_TEMP  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( LOC_A ) ,  (int) ( (LOC_B - LOC_A) ) )  ) ; 
                    __context__.SourceCodeLine = 173;
                    LOC_A = (ushort) ( (Functions.Find( "<UserName>" , DATA ) + 10) ) ; 
                    __context__.SourceCodeLine = 174;
                    LOC_B = (ushort) ( Functions.Find( "</" , DATA , LOC_A ) ) ; 
                    __context__.SourceCodeLine = 176;
                    ADMIN  .UpdateValue ( Functions.Mid ( DATA ,  (int) ( LOC_A ) ,  (int) ( (LOC_B - LOC_A) ) )  ) ; 
                    __context__.SourceCodeLine = 179;
                    MESSAGE_RECIEVED  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 183;
                    FMT_MSG  .UpdateValue ( "<FONT size=\u002216\u0022>" + Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000D</FONT>" + "<FONT size=\u002220\u0022>" + ADMIN + " to You: " + MSG_TEMP + "\u000D\u000d</FONT>"  ) ; 
                    __context__.SourceCodeLine = 185;
                    UNFORMAT_MSG_TEMP  .UpdateValue ( Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000D" + ADMIN + " to You: " + MSG_TEMP + "\u000D\u000d"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 187;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "close" , DATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 190;
                        MESSAGE_RECIEVED  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 193;
                        FMT_MSG  .UpdateValue ( "<FONT size=\u002216\u0022>" + Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000D</FONT>" + "<FONT size=\u002220\u0022>Request Closed.\u000D\u000d</FONT>"  ) ; 
                        __context__.SourceCodeLine = 195;
                        UNFORMAT_MSG_TEMP  .UpdateValue ( Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000DRequest Closed.\u000D\u000d"  ) ; 
                        __context__.SourceCodeLine = 197;
                        ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
                        ushort __FN_FOREND_VAL__1 = (ushort)OPENIDS; 
                        int __FN_FORSTEP_VAL__1 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                            { 
                            __context__.SourceCodeLine = 199;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_SplusNVRAM.REQ_ID[ I ] == CURRENTID))  ) ) 
                                { 
                                __context__.SourceCodeLine = 201;
                                _SplusNVRAM.REQ_ID [ I ]  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 202;
                                OPENIDS = (ushort) ( (OPENIDS - 1) ) ; 
                                __context__.SourceCodeLine = 203;
                                break ; 
                                } 
                            
                            __context__.SourceCodeLine = 197;
                            } 
                        
                        __context__.SourceCodeLine = 207;
                        ushort __FN_FORSTART_VAL__2 = (ushort) ( I ) ;
                        ushort __FN_FOREND_VAL__2 = (ushort)OPENIDS; 
                        int __FN_FORSTEP_VAL__2 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                            { 
                            __context__.SourceCodeLine = 209;
                            _SplusNVRAM.REQ_ID [ I ]  .UpdateValue ( _SplusNVRAM.REQ_ID [ (I + 1) ]  ) ; 
                            __context__.SourceCodeLine = 207;
                            } 
                        
                        __context__.SourceCodeLine = 213;
                        CreateWait ( "RESOLVE1" , 500 , RESOLVE1_Callback ) ;
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 219;
                return ( FMT_MSG ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 223;
                return ( "" ) ; 
                } 
            
            
            return ""; // default return value (none specified in module)
            }
            
        public void RESOLVE1_CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 215;
            Functions.Pulse ( 10, REQUEST_RESOLVED ) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    object CANCEL_MESSAGE_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 232;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_SplusNVRAM.REQ_ID[ OPENIDS ] != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 235;
                FUSION_HELP_REQUEST  .UpdateValue ( "<HelpRequest>" + "<ID>" + _SplusNVRAM.REQ_ID [ 1 ] + "</ID>" + "<Message></Message>" + "<Severity>1</Severity>" + "<Type>cancel</Type>" + "</HelpRequest>"  ) ; 
                __context__.SourceCodeLine = 242;
                _SplusNVRAM.REQ_ID [ OPENIDS ]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 243;
                OPENIDS = (ushort) ( (OPENIDS - 1) ) ; 
                __context__.SourceCodeLine = 246;
                CreateWait ( "RESOLVE2" , 500 , RESOLVE2_Callback ) ;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void RESOLVE2_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 248;
            Functions.Pulse ( 10, REQUEST_RESOLVED ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object SEND_MESSAGE_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort I = 0;
        
        CrestronString TEMP_MESSAGE;
        CrestronString TIME_TEMP;
        TEMP_MESSAGE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 250, this );
        TIME_TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        
        
        __context__.SourceCodeLine = 259;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( UNFORMATTED_MESSAGE ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 264;
            CancelWait ( "RESOLVE1" ) ; 
            __context__.SourceCodeLine = 265;
            CancelWait ( "RESOLVE2" ) ; 
            __context__.SourceCodeLine = 267;
            OPENIDS = (ushort) ( (OPENIDS + 1) ) ; 
            __context__.SourceCodeLine = 269;
            _SplusNVRAM.REQ_ID [ OPENIDS ]  .UpdateValue ( MAKE_THE_ID (  __context__  )  ) ; 
            __context__.SourceCodeLine = 272;
            TIME_TEMP  .UpdateValue ( MAKE_THE_TIME (  __context__  )  ) ; 
            __context__.SourceCodeLine = 273;
            TEMP_MESSAGE  .UpdateValue ( UNFORMATTED_MESSAGE  ) ; 
            __context__.SourceCodeLine = 274;
            Functions.ClearBuffer ( UNFORMATTED_MESSAGE ) ; 
            __context__.SourceCodeLine = 277;
            FUSION_HELP_REQUEST  .UpdateValue ( "<HelpRequest>" + "<ID>" + _SplusNVRAM.REQ_ID [ OPENIDS ] + "</ID>" + "<Message>" + TEMP_MESSAGE + "</Message>" + "<Severity>1</Severity>" + "<Type>new_user</Type>" + "</HelpRequest>"  ) ; 
            __context__.SourceCodeLine = 284;
            FORMATTED_RESPONSE  .UpdateValue ( "<FONT size=\u002216\u0022>" + Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000D</FONT>" + "<FONT size=\u002220\u0022>You to Admin: " + TEMP_MESSAGE + "\u000D\u000d</FONT>"  ) ; 
            __context__.SourceCodeLine = 286;
            UNFORMAT_MSG_TEMP  .UpdateValue ( Functions.Date (  (int) ( DATE_FORMAT  .Value ) ) + "\u0020" + TIME_TEMP + "\u000DYou to Admin: " + TEMP_MESSAGE + "\u000D\u000d"  ) ; 
            __context__.SourceCodeLine = 287;
            UNFORMATTED_RESPONSE  .UpdateValue ( UNFORMAT_MSG_TEMP  ) ; 
            __context__.SourceCodeLine = 290;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)OPENIDS; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 292;
                OPEN_REQUESTS  .UpdateValue ( _SplusNVRAM.REQ_ID [ I ]  ) ; 
                __context__.SourceCodeLine = 290;
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object UNFORMATTED_MESSAGE_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 299;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( UNFORMATTED_MESSAGE ) > 200 ))  ) ) 
            { 
            __context__.SourceCodeLine = 301;
            Functions.ClearBuffer ( UNFORMATTED_MESSAGE ) ; 
            __context__.SourceCodeLine = 302;
            Trace( "Bad Wolf") ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FUSION_HELP_RESPONSE_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString RET;
        RET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
        
        
        __context__.SourceCodeLine = 310;
        while ( Functions.TestForTrue  ( ( 1)  ) ) 
            { 
            __context__.SourceCodeLine = 312;
            RET  .UpdateValue ( Functions.Gather ( "</HelpRequest>" , FUSION_HELP_RESPONSE )  ) ; 
            __context__.SourceCodeLine = 313;
            FORMATTED_RESPONSE  .UpdateValue ( FORMAT_THE_RESPONSE (  __context__ , RET)  ) ; 
            __context__.SourceCodeLine = 314;
            UNFORMATTED_RESPONSE  .UpdateValue ( UNFORMAT_MSG_TEMP  ) ; 
            __context__.SourceCodeLine = 310;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort INDEX = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 322;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 323;
        OPENIDS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 325;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( _SplusNVRAM.REQ_ID[ 0 ] ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 327;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (_SplusNVRAM.REQ_ID[ 0 ] != "RaZoR"))  ) ) 
                { 
                __context__.SourceCodeLine = 329;
                _SplusNVRAM.REQ_ID [ 0 ]  .UpdateValue ( "RaZoR"  ) ; 
                __context__.SourceCodeLine = 330;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)255; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( INDEX  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (INDEX  >= __FN_FORSTART_VAL__1) && (INDEX  <= __FN_FOREND_VAL__1) ) : ( (INDEX  <= __FN_FORSTART_VAL__1) && (INDEX  >= __FN_FOREND_VAL__1) ) ; INDEX  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 332;
                    _SplusNVRAM.REQ_ID [ INDEX ]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 330;
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 337;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)255; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( INDEX  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (INDEX  >= __FN_FORSTART_VAL__2) && (INDEX  <= __FN_FOREND_VAL__2) ) : ( (INDEX  <= __FN_FORSTART_VAL__2) && (INDEX  >= __FN_FOREND_VAL__2) ) ; INDEX  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 339;
                    _SplusNVRAM.REQ_ID [ INDEX ]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 337;
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    UNFORMAT_MSG_TEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 150, this );
    _SplusNVRAM.REQ_ID  = new CrestronString[ 256 ];
    for( uint i = 0; i < 256; i++ )
        _SplusNVRAM.REQ_ID [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    
    SEND_MESSAGE = new Crestron.Logos.SplusObjects.DigitalInput( SEND_MESSAGE__DigitalInput__, this );
    m_DigitalInputList.Add( SEND_MESSAGE__DigitalInput__, SEND_MESSAGE );
    
    CANCEL_MESSAGE = new Crestron.Logos.SplusObjects.DigitalInput( CANCEL_MESSAGE__DigitalInput__, this );
    m_DigitalInputList.Add( CANCEL_MESSAGE__DigitalInput__, CANCEL_MESSAGE );
    
    MESSAGE_RECIEVED = new Crestron.Logos.SplusObjects.DigitalOutput( MESSAGE_RECIEVED__DigitalOutput__, this );
    m_DigitalOutputList.Add( MESSAGE_RECIEVED__DigitalOutput__, MESSAGE_RECIEVED );
    
    REQUEST_RESOLVED = new Crestron.Logos.SplusObjects.DigitalOutput( REQUEST_RESOLVED__DigitalOutput__, this );
    m_DigitalOutputList.Add( REQUEST_RESOLVED__DigitalOutput__, REQUEST_RESOLVED );
    
    UNFORMATTED_MESSAGE = new Crestron.Logos.SplusObjects.StringInput( UNFORMATTED_MESSAGE__AnalogSerialInput__, 250, this );
    m_StringInputList.Add( UNFORMATTED_MESSAGE__AnalogSerialInput__, UNFORMATTED_MESSAGE );
    
    OPEN_REQUESTS = new Crestron.Logos.SplusObjects.StringOutput( OPEN_REQUESTS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( OPEN_REQUESTS__AnalogSerialOutput__, OPEN_REQUESTS );
    
    UNFORMATTED_RESPONSE = new Crestron.Logos.SplusObjects.StringOutput( UNFORMATTED_RESPONSE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( UNFORMATTED_RESPONSE__AnalogSerialOutput__, UNFORMATTED_RESPONSE );
    
    FUSION_HELP_REQUEST = new Crestron.Logos.SplusObjects.StringOutput( FUSION_HELP_REQUEST__AnalogSerialOutput__, this );
    m_StringOutputList.Add( FUSION_HELP_REQUEST__AnalogSerialOutput__, FUSION_HELP_REQUEST );
    
    FORMATTED_RESPONSE = new Crestron.Logos.SplusObjects.StringOutput( FORMATTED_RESPONSE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( FORMATTED_RESPONSE__AnalogSerialOutput__, FORMATTED_RESPONSE );
    
    FUSION_HELP_RESPONSE = new Crestron.Logos.SplusObjects.BufferInput( FUSION_HELP_RESPONSE__AnalogSerialInput__, 1000, this );
    m_StringInputList.Add( FUSION_HELP_RESPONSE__AnalogSerialInput__, FUSION_HELP_RESPONSE );
    
    DATE_FORMAT = new UShortParameter( DATE_FORMAT__Parameter__, this );
    m_ParameterList.Add( DATE_FORMAT__Parameter__, DATE_FORMAT );
    
    TIME_FORMAT = new UShortParameter( TIME_FORMAT__Parameter__, this );
    m_ParameterList.Add( TIME_FORMAT__Parameter__, TIME_FORMAT );
    
    RESOLVE1_Callback = new WaitFunction( RESOLVE1_CallbackFn );
    RESOLVE2_Callback = new WaitFunction( RESOLVE2_CallbackFn );
    
    CANCEL_MESSAGE.OnDigitalPush.Add( new InputChangeHandlerWrapper( CANCEL_MESSAGE_OnPush_0, false ) );
    SEND_MESSAGE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SEND_MESSAGE_OnPush_1, false ) );
    UNFORMATTED_MESSAGE.OnSerialChange.Add( new InputChangeHandlerWrapper( UNFORMATTED_MESSAGE_OnChange_2, false ) );
    FUSION_HELP_RESPONSE.OnSerialChange.Add( new InputChangeHandlerWrapper( FUSION_HELP_RESPONSE_OnChange_3, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public CrestronModuleClass_FUSION_SSI_BASIC_HELP_TRANSLATOR_V1_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction RESOLVE1_Callback;
private WaitFunction RESOLVE2_Callback;


const uint SEND_MESSAGE__DigitalInput__ = 0;
const uint CANCEL_MESSAGE__DigitalInput__ = 1;
const uint UNFORMATTED_MESSAGE__AnalogSerialInput__ = 0;
const uint FUSION_HELP_RESPONSE__AnalogSerialInput__ = 1;
const uint MESSAGE_RECIEVED__DigitalOutput__ = 0;
const uint REQUEST_RESOLVED__DigitalOutput__ = 1;
const uint OPEN_REQUESTS__AnalogSerialOutput__ = 0;
const uint UNFORMATTED_RESPONSE__AnalogSerialOutput__ = 1;
const uint FUSION_HELP_REQUEST__AnalogSerialOutput__ = 2;
const uint FORMATTED_RESPONSE__AnalogSerialOutput__ = 3;
const uint DATE_FORMAT__Parameter__ = 10;
const uint TIME_FORMAT__Parameter__ = 11;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    [SplusStructAttribute(0, false, true)]
            public CrestronString [] REQ_ID;
            
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
