using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using SSFileOps;

namespace CrestronModule_FUSION_SSI_GUID_UPDATER_HELPER_V1_0_3
{
    public class CrestronModuleClass_FUSION_SSI_GUID_UPDATER_HELPER_V1_0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput UPDATE_GUIDS_B;
        Crestron.Logos.SplusObjects.StringInput MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__;
        Crestron.Logos.SplusObjects.BufferInput CONSOLE_RX__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput UPDATE_GUID_FINISHED;
        Crestron.Logos.SplusObjects.StringOutput UPDATE_GUID_STATUS_TXT__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput MASTER_GUID_PREFIX_TXT__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput RVI_FILE_NAME_TXT__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput RVI_FILE_FULL_PATH_TXT__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput CONSOLE_TX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput CONSOLE_STATUS_TXT__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput ROOMNAME__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput ROOMGUID__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput SYMBOL_SLOT_GUIDS_TX__DOLLAR__;
        UShortParameter PROCESSORMODE;
        UShortParameter PREFIXGUIDS;
        UShortParameter THREESERIESAPPENDSLOTNUMBER;
        StringParameter FILESTORAGELOCATION__DOLLAR__;
        SplusTcpClient TCPCONSOLE;
        SSFileOps.SimplSharpFileOps FILEOPS;
        ushort G_NDEBUG = 0;
        ushort G_NDEBUGRVIFILE = 0;
        ushort G_NCONSOLESTEP = 0;
        ushort G_NFUSIONDATASTARTED = 0;
        ushort G_NFUSIONSYMBOLDATASTARTED = 0;
        ushort G_NFUSIONSYMBOLCOUNT = 0;
        ushort G_NSLOTNUMBER = 0;
        ushort G_NINSTANCEIDFOUND = 0;
        ushort G_NTSIDRETRYCOUNT = 0;
        ushort G_NTSIDUPDATEBUSY = 0;
        ushort G_NINITIALRUN = 0;
        ushort G_NRVIFILECHANGED = 0;
        ushort G_NROOMNAMEOVERRIDEALLOWED = 0;
        ushort G_NMASTERGUIDOVERRIDE = 0;
        ushort [] G_NGUIDOVERRIDE;
        ushort [] G_NROOMNAMEUPDATED;
        ushort [] G_NGUIDCOUNT;
        short G_SNCONSOLECONNECTIONOK = 0;
        short G_SNCONSOLECONNECTIONSTATUS = 0;
        ushort DBUG = 0;
        ushort BADSTORAGEPATH = 0;
        ushort INITIALIZED = 0;
        ushort UPDATEGUIDRECEIVED = 0;
        CrestronString G_SMASTERGUIDPREFIX__DOLLAR__;
        CrestronString [] G_SGUIDPREFIX__DOLLAR__;
        CrestronString G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__;
        CrestronString G_SRVITEMP__DOLLAR__;
        CrestronString G_SRVINEWFILETEMP__DOLLAR__;
        CrestronString G_SRVIFULLFILEPATH__DOLLAR__;
        CrestronString G_SRVINEWFULLFILEPATH__DOLLAR__;
        CrestronString G_SRVIFILENAME__DOLLAR__;
        CrestronString G_SRVIFILELOCATION__DOLLAR__;
        CrestronString [] G_SOVERRIDEROOMNAME__DOLLAR__;
        CrestronString FILESTORAGELOCATION;
        private CrestronString UPDATEGUIDPREFIX (  SplusExecutionContext __context__, CrestronString SCURRENTGUIDPREFIX__DOLLAR__ , ushort NGUIDSYMBOLNUMBER ) 
            { 
            ushort NPROGSLOT2 = 0;
            
            CrestronString SNEWGUIDPREFIX__DOLLAR__;
            SNEWGUIDPREFIX__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 12, this );
            
            
            __context__.SourceCodeLine = 217;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 217;
                Trace( "***** Start UpdateGUIDPrefix *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 218;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 218;
                Trace( "sCurrentGUIDPrefix$ = {0}\r\n", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 219;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 219;
                Trace( "nGUIDSymbolNumber = {0:d}\r\n", (short)NGUIDSYMBOLNUMBER) ; 
                }
            
            __context__.SourceCodeLine = 221;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NGUIDSYMBOLNUMBER == 1000))  ) ) 
                { 
                __context__.SourceCodeLine = 223;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 223;
                    Trace( "Inside if(nGUIDSymbolNumber = cnGUIDMasterOverride)\r\n") ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 225;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NGUIDSYMBOLNUMBER == 1001))  ) ) 
                    { 
                    __context__.SourceCodeLine = 227;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 227;
                        Trace( "Inside else if(nGUIDSymbolNumber = cnGUIDConsoleOverride)\r\n") ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 229;
                    if ( Functions.TestForTrue  ( ( G_NGUIDOVERRIDE[ NGUIDSYMBOLNUMBER ])  ) ) 
                        { 
                        __context__.SourceCodeLine = 231;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 231;
                            Trace( "g_nGUIDOverride[nGUIDSymbolNumber] = {0:d}\r\n", (short)G_NGUIDOVERRIDE[ NGUIDSYMBOLNUMBER ]) ; 
                            }
                        
                        __context__.SourceCodeLine = 232;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 232;
                            Trace( "Inside if(g_nGUIDOverride[nGUIDSymbolNumber]), g_sGUIDPrefix$ = {0}\r\n", G_SGUIDPREFIX__DOLLAR__ [ NGUIDSYMBOLNUMBER ] ) ; 
                            }
                        
                        __context__.SourceCodeLine = 233;
                        MakeString ( SCURRENTGUIDPREFIX__DOLLAR__ , "{0}", G_SGUIDPREFIX__DOLLAR__ [ NGUIDSYMBOLNUMBER ] ) ; 
                        } 
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 236;
            
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)Functions.GetSeries());
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 240;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 240;
                            Trace( "2 Series Processor\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 241;
                        MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 245;
                        NPROGSLOT2 = (ushort) ( GetProgramNumber() ) ; 
                        __context__.SourceCodeLine = 247;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 247;
                            Trace( "3 Series Processor\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 248;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 248;
                            Trace( "nProgSlot2 = {0:d}\r\n", (ushort)NPROGSLOT2) ; 
                            }
                        
                        __context__.SourceCodeLine = 250;
                        if ( Functions.TestForTrue  ( ( THREESERIESAPPENDSLOTNUMBER  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 252;
                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 252;
                                Trace( "Inside if(ThreeSeriesAppendSlotNumber)\r\n") ; 
                                }
                            
                            __context__.SourceCodeLine = 253;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 1))  ) ) 
                                { 
                                __context__.SourceCodeLine = 255;
                                MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-01", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 257;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 2))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 259;
                                    MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-02", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 261;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 3))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 263;
                                        MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-03", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 265;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 4))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 267;
                                            MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-04", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 269;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 5))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 271;
                                                MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-05", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 273;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 6))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 275;
                                                    MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-06", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 277;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 7))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 279;
                                                        MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-07", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 281;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 8))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 283;
                                                            MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-08", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 285;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 9))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 287;
                                                                MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-09", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 289;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NPROGSLOT2 == 10))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 291;
                                                                    MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}-10", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 296;
                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 296;
                                Trace( "Inside ELSE FOR if(ThreeSeriesAppendSlotNumber)\r\n") ; 
                                }
                            
                            __context__.SourceCodeLine = 297;
                            MakeString ( SNEWGUIDPREFIX__DOLLAR__ , "{0}", SCURRENTGUIDPREFIX__DOLLAR__ ) ; 
                            } 
                        
                        } 
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 302;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 302;
                Trace( "sNewGUIDPrefix$ = {0}\r\n", SNEWGUIDPREFIX__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 303;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 303;
                Trace( "***** End UpdateGUIDPrefix *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 305;
            return ( SNEWGUIDPREFIX__DOLLAR__ ) ; 
            
            }
            
        private void ACQUIREAPPPATH (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 310;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Determining Processor Type...") ; 
            __context__.SourceCodeLine = 311;
            MakeString ( G_SRVIFILELOCATION__DOLLAR__ , "{0}{1}", FILEOPS . GetApplicationPath ( ) , "\\" ) ; 
            __context__.SourceCodeLine = 312;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 312;
                Trace( "app path: {0}\r\n", G_SRVIFILELOCATION__DOLLAR__ ) ; 
                }
            
            
            }
            
        private void SEEKRVIFILE (  SplusExecutionContext __context__ ) 
            { 
            CrestronString RVIFILENAMEFOUND;
            RVIFILENAMEFOUND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 319;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 319;
                Trace( "***** Start Find RVI File *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 321;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Finding RVI File...") ; 
            __context__.SourceCodeLine = 323;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 323;
                Trace( "path: {0} \r\n", G_SRVIFILELOCATION__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 324;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 324;
                Trace( "ext: {0} \r\n", "*.rvi" ) ; 
                }
            
            __context__.SourceCodeLine = 326;
            RVIFILENAMEFOUND  .UpdateValue ( FILEOPS . GetFirstFileNameByExt (  G_SRVIFILELOCATION__DOLLAR__  .ToString() , "*.rvi")  ) ; 
            __context__.SourceCodeLine = 328;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 328;
                Trace( "rviFileNameFound = {0}\r\n", RVIFILENAMEFOUND ) ; 
                }
            
            __context__.SourceCodeLine = 329;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( RVIFILENAMEFOUND ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 331;
                MakeString ( G_SRVIFILENAME__DOLLAR__ , "{0}{1}", RVIFILENAMEFOUND , ".rvi" ) ; 
                __context__.SourceCodeLine = 332;
                MakeString ( RVI_FILE_NAME_TXT__DOLLAR__ , "{0}", G_SRVIFILENAME__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 333;
                MakeString ( RVI_FILE_FULL_PATH_TXT__DOLLAR__ , "{0}{1}", G_SRVIFILELOCATION__DOLLAR__ , G_SRVIFILENAME__DOLLAR__ ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 337;
                MakeString ( G_SRVIFILENAME__DOLLAR__ , "{0}", "FileFoundError" ) ; 
                __context__.SourceCodeLine = 338;
                MakeString ( RVI_FILE_NAME_TXT__DOLLAR__ , "{0}", "FileFoundError" ) ; 
                __context__.SourceCodeLine = 339;
                MakeString ( RVI_FILE_FULL_PATH_TXT__DOLLAR__ , "{0}", "FileFoundError" ) ; 
                } 
            
            __context__.SourceCodeLine = 342;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 342;
                Trace( "g_sRVIFileName$ = {0}\r\n", G_SRVIFILENAME__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 343;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 343;
                Trace( "g_sRVIFileLocation$ = {0}\r\n", G_SRVIFILELOCATION__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 344;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 344;
                Trace( "***** End Find RVI File *****\r\n") ; 
                }
            
            
            }
            
        private void WRITETEMPFILE (  SplusExecutionContext __context__ ) 
            { 
            CrestronString SFILEWRITEPATH__DOLLAR__;
            SFILEWRITEPATH__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            ushort NFILELEN = 0;
            
            short SNFILEHANDLE = 0;
            short SNNUMWRITE = 0;
            short SNSTARTFILEIOERROR = 0;
            short SNENDFILEIOERROR = 0;
            short SNFILECLOSEERROR = 0;
            
            
            __context__.SourceCodeLine = 353;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 353;
                Trace( "***** Start WriteTempFile *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 355;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Writing Data To File...") ; 
            __context__.SourceCodeLine = 357;
            MakeString ( SFILEWRITEPATH__DOLLAR__ , "{0}{1}{2}{3:d}", FILESTORAGELOCATION , G_SRVIFILENAME__DOLLAR__ , "_tmp" , (short)GetProgramNumber()) ; 
            __context__.SourceCodeLine = 362;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 362;
                Trace( "sFileWritePath$ = {0}\r\n", SFILEWRITEPATH__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 364;
            SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
            __context__.SourceCodeLine = 365;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNSTARTFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 367;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 367;
                    Trace( "Start File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 368;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 369;
                SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
                __context__.SourceCodeLine = 365;
                } 
            
            __context__.SourceCodeLine = 372;
            if ( Functions.TestForTrue  ( ( G_NDEBUGRVIFILE)  ) ) 
                {
                __context__.SourceCodeLine = 373;
                SNFILEHANDLE = (short) ( FileOpenShared( "\\NVRAM\\ADG-PV-Room_230-140310.rvi" ,(ushort) (((1 | 256) | 8) | 16384) ) ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 375;
                SNFILEHANDLE = (short) ( FileOpenShared( SFILEWRITEPATH__DOLLAR__ ,(ushort) (((1 | 256) | 8) | 16384) ) ) ; 
                }
            
            __context__.SourceCodeLine = 377;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                { 
                __context__.SourceCodeLine = 379;
                Trace( "FileOpen Complete\r\n") ; 
                __context__.SourceCodeLine = 380;
                Trace( "snFileHandle = {0:d}\r\n", (short)SNFILEHANDLE) ; 
                } 
            
            __context__.SourceCodeLine = 383;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( SNFILEHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 385;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 385;
                    Trace( "***** Inside if(snFileHandle >= 0) *****\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 386;
                NFILELEN = (ushort) ( Functions.Length( G_SRVINEWFILETEMP__DOLLAR__ ) ) ; 
                __context__.SourceCodeLine = 387;
                SNNUMWRITE = (short) ( FileWrite( (short)( SNFILEHANDLE ) , G_SRVINEWFILETEMP__DOLLAR__ , (ushort)( NFILELEN ) ) ) ; 
                __context__.SourceCodeLine = 388;
                SNFILECLOSEERROR = (short) ( FileClose( (short)( SNFILEHANDLE ) ) ) ; 
                __context__.SourceCodeLine = 389;
                while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNFILECLOSEERROR != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 391;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 391;
                        Trace( "File Close Error Retrying\r\n") ; 
                        }
                    
                    __context__.SourceCodeLine = 392;
                    Functions.Delay (  (int) ( 100 ) ) ; 
                    __context__.SourceCodeLine = 393;
                    SNFILECLOSEERROR = (short) ( FileClose( (short)( SNFILEHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 389;
                    } 
                
                __context__.SourceCodeLine = 396;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 396;
                    Trace( "nFileLen = {0:d}\r\n", (short)NFILELEN) ; 
                    }
                
                __context__.SourceCodeLine = 397;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 397;
                    Trace( "snNumWrite = {0:d}\r\n", (short)SNNUMWRITE) ; 
                    }
                
                __context__.SourceCodeLine = 399;
                Functions.ClearBuffer ( G_SRVINEWFILETEMP__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 401;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( G_NDEBUG ) && Functions.TestForTrue ( Functions.BoolToInt ( SNNUMWRITE < 0 ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 401;
                    Trace( "Error Writing to File\r\n") ; 
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 405;
                BADSTORAGEPATH = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 406;
                GenerateUserError ( "Fusion GUID Updater Invalid storage location, check module storage parameter. {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                } 
            
            __context__.SourceCodeLine = 408;
            SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
            __context__.SourceCodeLine = 409;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNENDFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 411;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 411;
                    Trace( "End File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 412;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 413;
                SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
                __context__.SourceCodeLine = 409;
                } 
            
            __context__.SourceCodeLine = 416;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 416;
                Trace( "***** End WriteTempFile *****\r\n") ; 
                }
            
            
            }
            
        private CrestronString ADDNEWFILEDATA (  SplusExecutionContext __context__, CrestronString SCURRENTDATA__DOLLAR__ ) 
            { 
            
            __context__.SourceCodeLine = 421;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_SRVINEWFILETEMP__DOLLAR__ != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 423;
                MakeString ( G_SRVINEWFILETEMP__DOLLAR__ , "{0}{1}", G_SRVINEWFILETEMP__DOLLAR__ , SCURRENTDATA__DOLLAR__ ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 427;
                MakeString ( G_SRVINEWFILETEMP__DOLLAR__ , "{0}", SCURRENTDATA__DOLLAR__ ) ; 
                } 
            
            
            return ""; // default return value (none specified in module)
            }
            
        private CrestronString GETROOMNAME (  SplusExecutionContext __context__, ushort NCURRENTSYMBOLCOUNT , CrestronString SCURRENTDATA__DOLLAR__ ) 
            { 
            ushort NSTARTPOSITION = 0;
            ushort NENDPOSITION = 0;
            ushort NCOUNT = 0;
            
            CrestronString SROOMNAMERETURN__DOLLAR__;
            SROOMNAMERETURN__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 436;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NCURRENTSYMBOLCOUNT <= 60 ))  ) ) 
                { 
                __context__.SourceCodeLine = 438;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_SOVERRIDEROOMNAME__DOLLAR__[ NCURRENTSYMBOLCOUNT ] != ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 440;
                    MakeString ( SROOMNAMERETURN__DOLLAR__ , "{0}", G_SOVERRIDEROOMNAME__DOLLAR__ [ NCURRENTSYMBOLCOUNT ] ) ; 
                    __context__.SourceCodeLine = 441;
                    G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 442;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 442;
                        Trace( "Line 451 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                        }
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 446;
                    NSTARTPOSITION = (ushort) ( (Functions.Find( "<RoomName>" , SCURRENTDATA__DOLLAR__ ) + Functions.Length( "<RoomName>" )) ) ; 
                    __context__.SourceCodeLine = 447;
                    NENDPOSITION = (ushort) ( Functions.Find( "</RoomName>" , SCURRENTDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                    __context__.SourceCodeLine = 448;
                    NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                    __context__.SourceCodeLine = 449;
                    MakeString ( SROOMNAMERETURN__DOLLAR__ , "{0}", Functions.Mid ( SCURRENTDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 454;
                NSTARTPOSITION = (ushort) ( (Functions.Find( "<RoomName>" , SCURRENTDATA__DOLLAR__ ) + Functions.Length( "<RoomName>" )) ) ; 
                __context__.SourceCodeLine = 455;
                NENDPOSITION = (ushort) ( Functions.Find( "</RoomName>" , SCURRENTDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                __context__.SourceCodeLine = 456;
                NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                __context__.SourceCodeLine = 457;
                MakeString ( SROOMNAMERETURN__DOLLAR__ , "{0}", Functions.Mid ( SCURRENTDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                } 
            
            __context__.SourceCodeLine = 460;
            return ( SROOMNAMERETURN__DOLLAR__ ) ; 
            
            }
            
        private CrestronString GETNODENAME (  SplusExecutionContext __context__, ushort NCURRENTSYMBOLCOUNT , CrestronString SCURRENTDATA__DOLLAR__ ) 
            { 
            ushort NSTARTPOSITION = 0;
            ushort NENDPOSITION = 0;
            ushort NCOUNT = 0;
            
            CrestronString SNODENAMERETURN__DOLLAR__;
            SNODENAMERETURN__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 468;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NCURRENTSYMBOLCOUNT <= 60 ))  ) ) 
                { 
                __context__.SourceCodeLine = 470;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_SOVERRIDEROOMNAME__DOLLAR__[ NCURRENTSYMBOLCOUNT ] != ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 472;
                    MakeString ( SNODENAMERETURN__DOLLAR__ , "{0}", G_SOVERRIDEROOMNAME__DOLLAR__ [ NCURRENTSYMBOLCOUNT ] ) ; 
                    __context__.SourceCodeLine = 473;
                    G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 474;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 474;
                        Trace( "Line 473 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                        }
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 478;
                    NSTARTPOSITION = (ushort) ( (Functions.Find( "<NodeName>" , SCURRENTDATA__DOLLAR__ ) + Functions.Length( "<NodeName>" )) ) ; 
                    __context__.SourceCodeLine = 479;
                    NENDPOSITION = (ushort) ( Functions.Find( "</NodeName>" , SCURRENTDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                    __context__.SourceCodeLine = 480;
                    NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                    __context__.SourceCodeLine = 481;
                    MakeString ( SNODENAMERETURN__DOLLAR__ , "{0}", Functions.Mid ( SCURRENTDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 486;
                NSTARTPOSITION = (ushort) ( (Functions.Find( "<NodeName>" , SCURRENTDATA__DOLLAR__ ) + Functions.Length( "<NodeName>" )) ) ; 
                __context__.SourceCodeLine = 487;
                NENDPOSITION = (ushort) ( Functions.Find( "</NodeName>" , SCURRENTDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                __context__.SourceCodeLine = 488;
                NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                __context__.SourceCodeLine = 489;
                MakeString ( SNODENAMERETURN__DOLLAR__ , "{0}", Functions.Mid ( SCURRENTDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                } 
            
            __context__.SourceCodeLine = 492;
            return ( SNODENAMERETURN__DOLLAR__ ) ; 
            
            }
            
        private void EXTRACTROOMGUID (  SplusExecutionContext __context__, CrestronString ROOMGUID ) 
            { 
            ushort POINTER1 = 0;
            ushort POINTER2 = 0;
            
            
            __context__.SourceCodeLine = 499;
            POINTER1 = (ushort) ( (Functions.Find( "<InstanceID>" , ROOMGUID ) + 12) ) ; 
            __context__.SourceCodeLine = 500;
            POINTER2 = (ushort) ( Functions.Find( "</InstanceID>" , ROOMGUID , POINTER1 ) ) ; 
            __context__.SourceCodeLine = 502;
            ROOMGUID__DOLLAR__  .UpdateValue ( Functions.Mid ( ROOMGUID ,  (int) ( POINTER1 ) ,  (int) ( (POINTER2 - POINTER1) ) )  ) ; 
            
            }
            
        private CrestronString PARSERVIFILEDATA (  SplusExecutionContext __context__, CrestronString SRVITEMPFILEDATA__DOLLAR__ ) 
            { 
            ushort NSTARTPOSITION = 0;
            ushort NENDPOSITION = 0;
            ushort NCOUNT = 0;
            
            CrestronString STEMPDATA__DOLLAR__;
            CrestronString SRVITEMPNEWFILEDATA__DOLLAR__;
            CrestronString STEMPROOMNAME__DOLLAR__;
            CrestronString STEMPROOMNAMEDATA__DOLLAR__;
            CrestronString STEMPROOMGUID__DOLLAR__;
            CrestronString STEMPINSTANCEGUID__DOLLAR__;
            STEMPDATA__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            SRVITEMPNEWFILEDATA__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5500, this );
            STEMPROOMNAME__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            STEMPROOMNAMEDATA__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            STEMPROOMGUID__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            STEMPINSTANCEGUID__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 512;
            NSTARTPOSITION = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 513;
            NENDPOSITION = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 514;
            NCOUNT = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 516;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Processing Data...") ; 
            __context__.SourceCodeLine = 518;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 518;
                Trace( "***** Start ParseRVIFile *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 520;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_SRVITEMP__DOLLAR__ != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 522;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 522;
                    Trace( "g_sRVITemp$ <> \u0022\u0022 = True\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 523;
                MakeString ( G_SRVITEMP__DOLLAR__ , "{0}{1}", G_SRVITEMP__DOLLAR__ , SRVITEMPFILEDATA__DOLLAR__ ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 527;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 527;
                    Trace( "g_sRVITemp$ <> \u0022\u0022 = False\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 528;
                MakeString ( G_SRVITEMP__DOLLAR__ , "{0}", SRVITEMPFILEDATA__DOLLAR__ ) ; 
                } 
            
            __context__.SourceCodeLine = 530;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 530;
                Trace( "len(g_sRVITemp$) = {0:d}\r\nlen(sRVITempFileData$) = {1:d}\r\n", (ushort)Functions.Length( G_SRVITEMP__DOLLAR__ ), (ushort)Functions.Length( SRVITEMPFILEDATA__DOLLAR__ )) ; 
                }
            
            __context__.SourceCodeLine = 532;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\r\n" , G_SRVITEMP__DOLLAR__ ))  ) ) 
                { 
                __context__.SourceCodeLine = 534;
                STEMPDATA__DOLLAR__  .UpdateValue ( Functions.Remove ( "\r\n" , G_SRVITEMP__DOLLAR__ )  ) ; 
                __context__.SourceCodeLine = 535;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "<SymbolInfo>" , STEMPDATA__DOLLAR__ ) ) || Functions.TestForTrue ( G_NFUSIONSYMBOLDATASTARTED )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 537;
                    G_NFUSIONSYMBOLDATASTARTED = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 538;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 538;
                        Trace( "sTempData$ = {0}\r\n", STEMPDATA__DOLLAR__ ) ; 
                        }
                    
                    __context__.SourceCodeLine = 539;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Not( Functions.Find( "</SymbolInfo>" , STEMPDATA__DOLLAR__ ) ) ) && Functions.TestForTrue ( Functions.Find( "\r\n" , STEMPDATA__DOLLAR__ ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 541;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "<RoomName>" , STEMPDATA__DOLLAR__ ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 543;
                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 543;
                                Trace( "**Room Name Segment***\r\n sTempData$ = {0}\r\n", STEMPDATA__DOLLAR__ ) ; 
                                }
                            
                            __context__.SourceCodeLine = 544;
                            MakeString ( STEMPROOMNAME__DOLLAR__ , "{0}", GETROOMNAME (  __context__ , (ushort)( G_NFUSIONSYMBOLCOUNT ), STEMPDATA__DOLLAR__) ) ; 
                            __context__.SourceCodeLine = 547;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_NFUSIONSYMBOLCOUNT == 1))  ) ) 
                                {
                                __context__.SourceCodeLine = 547;
                                ROOMNAME__DOLLAR__  .UpdateValue ( STEMPROOMNAME__DOLLAR__  ) ; 
                                }
                            
                            __context__.SourceCodeLine = 549;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT <= 60 ))  ) ) 
                                { 
                                } 
                            
                            __context__.SourceCodeLine = 553;
                            MakeString ( STEMPROOMNAMEDATA__DOLLAR__ , "          {0}{1}{2}\r\n", "<RoomName>" , STEMPROOMNAME__DOLLAR__ , "</RoomName>" ) ; 
                            __context__.SourceCodeLine = 554;
                            ADDNEWFILEDATA (  __context__ , STEMPROOMNAMEDATA__DOLLAR__) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 556;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "<NodeName>" , STEMPDATA__DOLLAR__ ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 558;
                                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                    {
                                    __context__.SourceCodeLine = 558;
                                    Trace( "**Node Name Segment***\r\n sTempData$ = {0}\r\n", STEMPDATA__DOLLAR__ ) ; 
                                    }
                                
                                __context__.SourceCodeLine = 559;
                                MakeString ( STEMPROOMNAME__DOLLAR__ , "{0}", GETNODENAME (  __context__ , (ushort)( G_NFUSIONSYMBOLCOUNT ), STEMPDATA__DOLLAR__) ) ; 
                                __context__.SourceCodeLine = 560;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT <= 60 ))  ) ) 
                                    { 
                                    } 
                                
                                __context__.SourceCodeLine = 564;
                                MakeString ( STEMPROOMNAMEDATA__DOLLAR__ , "          {0}{1}{2}\r\n", "<NodeName>" , STEMPROOMNAME__DOLLAR__ , "</NodeName>" ) ; 
                                __context__.SourceCodeLine = 565;
                                ADDNEWFILEDATA (  __context__ , STEMPROOMNAMEDATA__DOLLAR__) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 567;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "<InstanceID>" , STEMPDATA__DOLLAR__ ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 569;
                                    NSTARTPOSITION = (ushort) ( (Functions.Find( "<InstanceID>" , STEMPDATA__DOLLAR__ ) + Functions.Length( "<InstanceID>" )) ) ; 
                                    __context__.SourceCodeLine = 570;
                                    NENDPOSITION = (ushort) ( Functions.Find( "</InstanceID>" , STEMPDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                                    __context__.SourceCodeLine = 571;
                                    NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                                    __context__.SourceCodeLine = 572;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT > 60 ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 574;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( G_SMASTERGUIDPREFIX__DOLLAR__ , STEMPDATA__DOLLAR__ ) ) || Functions.TestForTrue ( Functions.BoolToInt (PREFIXGUIDS  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 576;
                                            ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                            } 
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 580;
                                            G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 581;
                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                {
                                                __context__.SourceCodeLine = 581;
                                                Trace( "Line 715 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 582;
                                            MakeString ( STEMPROOMGUID__DOLLAR__ , "          {0}{1}-{2}{3}\r\n", "<InstanceID>" , G_SMASTERGUIDPREFIX__DOLLAR__ , Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) , "</InstanceID>" ) ; 
                                            __context__.SourceCodeLine = 583;
                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                {
                                                __context__.SourceCodeLine = 583;
                                                Trace( "sTempRoomGUID$ = {0}\r\n", STEMPROOMGUID__DOLLAR__ ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 584;
                                            ADDNEWFILEDATA (  __context__ , STEMPROOMGUID__DOLLAR__) ; 
                                            __context__.SourceCodeLine = 585;
                                            EXTRACTROOMGUID (  __context__ , STEMPROOMGUID__DOLLAR__) ; 
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 589;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( G_SGUIDPREFIX__DOLLAR__[ G_NFUSIONSYMBOLCOUNT ] , STEMPDATA__DOLLAR__ ) ) || Functions.TestForTrue ( Functions.BoolToInt (PREFIXGUIDS  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 592;
                                            ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                            } 
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 596;
                                            G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 597;
                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                {
                                                __context__.SourceCodeLine = 597;
                                                Trace( "Line 551 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 598;
                                            MakeString ( STEMPROOMGUID__DOLLAR__ , "          {0}{1}-{2}{3}\r\n", "<InstanceID>" , G_SGUIDPREFIX__DOLLAR__ [ G_NFUSIONSYMBOLCOUNT ] , Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) , "</InstanceID>" ) ; 
                                            __context__.SourceCodeLine = 599;
                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                {
                                                __context__.SourceCodeLine = 599;
                                                Trace( "sTempRoomGUID$ = {0}\r\n", STEMPROOMGUID__DOLLAR__ ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 601;
                                            ADDNEWFILEDATA (  __context__ , STEMPROOMGUID__DOLLAR__) ; 
                                            __context__.SourceCodeLine = 603;
                                            EXTRACTROOMGUID (  __context__ , STEMPROOMGUID__DOLLAR__) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 606;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "<IPID>" , STEMPDATA__DOLLAR__ ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 608;
                                        NSTARTPOSITION = (ushort) ( (Functions.Find( "<IPID>" , STEMPDATA__DOLLAR__ ) + Functions.Length( "<IPID>" )) ) ; 
                                        __context__.SourceCodeLine = 609;
                                        NENDPOSITION = (ushort) ( Functions.Find( "</IPID>" , STEMPDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                                        __context__.SourceCodeLine = 610;
                                        NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                                        __context__.SourceCodeLine = 611;
                                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                            {
                                            __context__.SourceCodeLine = 611;
                                            Trace( "**IP ID Segment***\r\n sTempData$ = {0}\r\n nStartPosition = {1:d}\r\n nEndPosition = {2:d}\r\n nCount = {3:d}\r\n", STEMPDATA__DOLLAR__ , (ushort)NSTARTPOSITION, (ushort)NENDPOSITION, (ushort)NCOUNT) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 612;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT <= 60 ))  ) ) 
                                            { 
                                            } 
                                        
                                        __context__.SourceCodeLine = 616;
                                        ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 618;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "<SlotNum>" , STEMPDATA__DOLLAR__ ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 620;
                                            NSTARTPOSITION = (ushort) ( (Functions.Find( "<SlotNum>" , STEMPDATA__DOLLAR__ ) + Functions.Length( "<SlotNum>" )) ) ; 
                                            __context__.SourceCodeLine = 621;
                                            NENDPOSITION = (ushort) ( Functions.Find( "</SlotNum>" , STEMPDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                                            __context__.SourceCodeLine = 622;
                                            NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                                            __context__.SourceCodeLine = 623;
                                            G_NSLOTNUMBER = (ushort) ( Functions.Atoi( Functions.Mid( STEMPDATA__DOLLAR__ , (int)( NSTARTPOSITION ) , (int)( NCOUNT ) ) ) ) ; 
                                            __context__.SourceCodeLine = 624;
                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                {
                                                __context__.SourceCodeLine = 624;
                                                Trace( "**Slot Number Segment***\r\n sTempData$ = {0}\r\n nStartPosition = {1:d}\r\n nEndPosition = {2:d}\r\n nCount = {3:d}\r\n g_nSlotNumber = {4:d}\r\n", STEMPDATA__DOLLAR__ , (ushort)NSTARTPOSITION, (ushort)NENDPOSITION, (ushort)NCOUNT, (ushort)G_NSLOTNUMBER) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 625;
                                            ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 640;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "<Name>InstanceID</Name>" , STEMPDATA__DOLLAR__ ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 642;
                                                G_NINSTANCEIDFOUND = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 643;
                                                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 643;
                                                    Trace( "***csParamSeachType Found***\r\n") ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 644;
                                                ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 646;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( G_NINSTANCEIDFOUND ) && Functions.TestForTrue ( Functions.Find( "<Value>" , STEMPDATA__DOLLAR__ ) )) ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 648;
                                                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 648;
                                                        Trace( "***else if(g_nInstanceIDFound && find(csParamGUIDStartofData, sTempData$)) Found***\r\n") ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 649;
                                                    NSTARTPOSITION = (ushort) ( (Functions.Find( "<Value>" , STEMPDATA__DOLLAR__ ) + Functions.Length( "<Value>" )) ) ; 
                                                    __context__.SourceCodeLine = 650;
                                                    NENDPOSITION = (ushort) ( Functions.Find( "</Value>" , STEMPDATA__DOLLAR__ , NSTARTPOSITION ) ) ; 
                                                    __context__.SourceCodeLine = 651;
                                                    NCOUNT = (ushort) ( (NENDPOSITION - NSTARTPOSITION) ) ; 
                                                    __context__.SourceCodeLine = 653;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT <= 60 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 655;
                                                        G_NGUIDCOUNT [ G_NFUSIONSYMBOLCOUNT] = (ushort) ( (G_NGUIDCOUNT[ G_NFUSIONSYMBOLCOUNT ] + 1) ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 658;
                                                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 658;
                                                        Trace( "g_nFusionSymbolCount = {0:d}\r\n", (ushort)G_NFUSIONSYMBOLCOUNT) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 659;
                                                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 659;
                                                        Trace( "nSlotNumber = {0:d}\r\n", (ushort)G_NSLOTNUMBER) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 661;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NFUSIONSYMBOLCOUNT > 60 ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 663;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( G_SMASTERGUIDPREFIX__DOLLAR__ , STEMPDATA__DOLLAR__ ) ) || Functions.TestForTrue ( Functions.BoolToInt (PREFIXGUIDS  .Value == 0) )) ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 665;
                                                            ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                                            } 
                                                        
                                                        else 
                                                            { 
                                                            __context__.SourceCodeLine = 669;
                                                            G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 670;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 670;
                                                                Trace( "Line 787 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 671;
                                                            MakeString ( STEMPINSTANCEGUID__DOLLAR__ , "                    {0}{1}-{2}{3}\r\n", "<Value>" , G_SMASTERGUIDPREFIX__DOLLAR__ , Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) , "</Value>" ) ; 
                                                            __context__.SourceCodeLine = 672;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 672;
                                                                Trace( "sTempInstanceGUID$ = {0}\r\n", STEMPINSTANCEGUID__DOLLAR__ ) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 673;
                                                            ADDNEWFILEDATA (  __context__ , STEMPROOMGUID__DOLLAR__) ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 675;
                                                        G_NINSTANCEIDFOUND = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 677;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( G_SGUIDPREFIX__DOLLAR__[ G_NFUSIONSYMBOLCOUNT ] , STEMPDATA__DOLLAR__ ) ) || Functions.TestForTrue ( Functions.BoolToInt (PREFIXGUIDS  .Value == 0) )) ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 679;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 679;
                                                                Trace( "***if(find(g_sGUIDPrefix$[g_nFusionSymbolCount], sTempData$)) Found***\r\n") ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 681;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_NFUSIONSYMBOLCOUNT == 1))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 683;
                                                                MakeString ( SYMBOL_SLOT_GUIDS_TX__DOLLAR__ , "{0:d},{1}\r\n", (ushort)G_NSLOTNUMBER, Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                                                                } 
                                                            
                                                            __context__.SourceCodeLine = 685;
                                                            ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                                            } 
                                                        
                                                        else 
                                                            { 
                                                            __context__.SourceCodeLine = 689;
                                                            G_NRVIFILECHANGED = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 690;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 690;
                                                                Trace( "Line 803 g_nRVIFileChanged = {0:d}\r\n", (short)G_NRVIFILECHANGED) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 691;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 691;
                                                                Trace( "***if(find(g_sGUIDPrefix$[g_nFusionSymbolCount], sTempData$)) ELSE Found***\r\n") ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 692;
                                                            MakeString ( STEMPINSTANCEGUID__DOLLAR__ , "                    {0}{1}-{2}{3}\r\n", "<Value>" , G_SGUIDPREFIX__DOLLAR__ [ G_NFUSIONSYMBOLCOUNT ] , Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) , "</Value>" ) ; 
                                                            __context__.SourceCodeLine = 693;
                                                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 693;
                                                                Trace( "sTempInstanceGUID$ = {0}\r\n", STEMPINSTANCEGUID__DOLLAR__ ) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 695;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_NFUSIONSYMBOLCOUNT == 1))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 697;
                                                                MakeString ( SYMBOL_SLOT_GUIDS_TX__DOLLAR__ , "{0:d},{1}-{2}\r\n", (ushort)G_NSLOTNUMBER, G_SGUIDPREFIX__DOLLAR__ [ G_NFUSIONSYMBOLCOUNT ] , Functions.Mid ( STEMPDATA__DOLLAR__ ,  (int) ( NSTARTPOSITION ) ,  (int) ( NCOUNT ) ) ) ; 
                                                                } 
                                                            
                                                            } 
                                                        
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 700;
                                                    G_NINSTANCEIDFOUND = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 701;
                                                    ADDNEWFILEDATA (  __context__ , STEMPINSTANCEGUID__DOLLAR__) ; 
                                                    } 
                                                
                                                else 
                                                    { 
                                                    __context__.SourceCodeLine = 705;
                                                    ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                                                    } 
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        __context__.SourceCodeLine = 708;
                        STEMPDATA__DOLLAR__  .UpdateValue ( Functions.Remove ( "\r\n" , G_SRVITEMP__DOLLAR__ )  ) ; 
                        __context__.SourceCodeLine = 709;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 709;
                            Trace( "sTempData$ = {0}\r\n", STEMPDATA__DOLLAR__ ) ; 
                            }
                        
                        __context__.SourceCodeLine = 539;
                        } 
                    
                    __context__.SourceCodeLine = 711;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "</SymbolInfo>" , STEMPDATA__DOLLAR__ ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 713;
                        ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                        __context__.SourceCodeLine = 714;
                        G_NFUSIONSYMBOLDATASTARTED = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 715;
                        G_NFUSIONSYMBOLCOUNT = (ushort) ( (G_NFUSIONSYMBOLCOUNT + 1) ) ; 
                        } 
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 720;
                    ADDNEWFILEDATA (  __context__ , STEMPDATA__DOLLAR__) ; 
                    } 
                
                __context__.SourceCodeLine = 532;
                } 
            
            __context__.SourceCodeLine = 724;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREFIXGUIDS  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 726;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 726;
                    Trace( "Inside if(PrefixGUIDs = cnTrue)\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 727;
                WRITETEMPFILE (  __context__  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 731;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 731;
                    Trace( "Inside ELSE if(PrefixGUIDs = cnTrue)\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 732;
                Functions.ClearBuffer ( G_SRVINEWFILETEMP__DOLLAR__ ) ; 
                } 
            
            __context__.SourceCodeLine = 734;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 734;
                Trace( "***** End ParseRVIFile *****\r\n") ; 
                }
            
            
            return ""; // default return value (none specified in module)
            }
            
        private void PROCESSRVIFILE (  SplusExecutionContext __context__, CrestronString SRVIFILE__DOLLAR__ ) 
            { 
            short SNFILEHANDLE = 0;
            short SNSTARTFILEIOERROR = 0;
            short SNREADERROR = 0;
            short SNFILECLOSEERROR = 0;
            short SNENDFILEIOERROR = 0;
            
            CrestronString SRVITEMPREADDATA__DOLLAR__;
            SRVITEMPREADDATA__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5500, this );
            
            
            __context__.SourceCodeLine = 742;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 742;
                Trace( "***** Start ProcessRVIFile *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 743;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Processing RVI File...") ; 
            __context__.SourceCodeLine = 745;
            SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
            __context__.SourceCodeLine = 746;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNSTARTFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 748;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 748;
                    Trace( "Start File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 749;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 750;
                SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
                __context__.SourceCodeLine = 746;
                } 
            
            __context__.SourceCodeLine = 753;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SRVIFILE__DOLLAR__ != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 755;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 755;
                    Trace( "sRVIFile$ = {0}\r\n", SRVIFILE__DOLLAR__ ) ; 
                    }
                
                __context__.SourceCodeLine = 757;
                SNFILEHANDLE = (short) ( FileOpenShared( SRVIFILE__DOLLAR__ ,(ushort) (0 | 16384) ) ) ; 
                __context__.SourceCodeLine = 759;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    { 
                    __context__.SourceCodeLine = 761;
                    Trace( "FileOpen Complete\r\n") ; 
                    __context__.SourceCodeLine = 762;
                    Trace( "snFileHandle = {0:d}\r\n", (short)SNFILEHANDLE) ; 
                    } 
                
                __context__.SourceCodeLine = 765;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( SNFILEHANDLE >= 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 767;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( FileRead( (short)( SNFILEHANDLE ) , SRVITEMPREADDATA__DOLLAR__ , (ushort)( 5000 ) ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 769;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 769;
                            Trace( "File Read Ok\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 770;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 770;
                            Trace( "len(sRVITempReadData$) = {0:d}\r\n", (ushort)Functions.Length( SRVITEMPREADDATA__DOLLAR__ )) ; 
                            }
                        
                        __context__.SourceCodeLine = 771;
                        PARSERVIFILEDATA (  __context__ , SRVITEMPREADDATA__DOLLAR__) ; 
                        __context__.SourceCodeLine = 767;
                        } 
                    
                    __context__.SourceCodeLine = 773;
                    SNFILECLOSEERROR = (short) ( FileClose( (short)( SNFILEHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 774;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNFILECLOSEERROR != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 776;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 776;
                            Trace( "File Close Error Retrying\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 777;
                        Functions.Delay (  (int) ( 100 ) ) ; 
                        __context__.SourceCodeLine = 778;
                        SNFILECLOSEERROR = (short) ( FileClose( (short)( SNFILEHANDLE ) ) ) ; 
                        __context__.SourceCodeLine = 774;
                        } 
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 783;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 783;
                        Trace( "snFileHandle Error\r\n") ; 
                        }
                    
                    } 
                
                } 
            
            __context__.SourceCodeLine = 787;
            SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
            __context__.SourceCodeLine = 788;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNENDFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 790;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 790;
                    Trace( "End File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 791;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 792;
                SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
                __context__.SourceCodeLine = 788;
                } 
            
            __context__.SourceCodeLine = 795;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 795;
                Trace( "***** End ProcessRVIFile *****\r\n") ; 
                }
            
            
            }
            
        private ushort RVIMATCH (  SplusExecutionContext __context__ ) 
            { 
            CrestronString TMPFILEPATHNAME;
            TMPFILEPATHNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            CrestronString RVIFILEPATHNAME;
            RVIFILEPATHNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            FILE_INFO TMPFILEINFO;
            TMPFILEINFO  = new FILE_INFO();
            TMPFILEINFO .PopulateDefaults();
            
            FILE_INFO RVIFILEINFO;
            RVIFILEINFO  = new FILE_INFO();
            RVIFILEINFO .PopulateDefaults();
            
            short TMPFILEFOUND = 0;
            
            short RVIFILEFOUND = 0;
            
            short TMPFILEHANDLE = 0;
            
            short RVIFILEHANDLE = 0;
            
            CrestronString TMPBUFFER;
            TMPBUFFER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
            
            CrestronString RVIBUFFER;
            RVIBUFFER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, this );
            
            ushort TMPREAD = 0;
            
            ushort RVIREAD = 0;
            
            short TMPFILECLOSEERROR = 0;
            
            short RVIFILECLOSEERROR = 0;
            
            short FILEOPERR = 0;
            
            ushort MATCHING = 0;
            
            ushort MATCH = 0;
            
            short SNSTARTFILEIOERROR = 0;
            short SNENDFILEIOERROR = 0;
            
            CrestronString FILENAME;
            FILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            CrestronString APPPATH;
            APPPATH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 825;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 825;
                Trace( "*** modified rvi match check ***\r\n") ; 
                }
            
            __context__.SourceCodeLine = 827;
            APPPATH  .UpdateValue ( G_SRVIFILELOCATION__DOLLAR__  ) ; 
            __context__.SourceCodeLine = 828;
            MakeString ( FILENAME , "{0}{1}{2:d}", G_SRVIFILENAME__DOLLAR__ , "_tmp" , (short)GetProgramNumber()) ; 
            __context__.SourceCodeLine = 830;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 830;
                Trace( "file name: {0} file path: {1}\r\n", FILENAME , APPPATH ) ; 
                }
            
            __context__.SourceCodeLine = 832;
            SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
            __context__.SourceCodeLine = 833;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNSTARTFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 835;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 835;
                    Trace( "Start File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 836;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 837;
                SNSTARTFILEIOERROR = (short) ( StartFileOperations() ) ; 
                __context__.SourceCodeLine = 833;
                } 
            
            __context__.SourceCodeLine = 840;
            MakeString ( TMPFILEPATHNAME , "{0}{1}", FILESTORAGELOCATION , FILENAME ) ; 
            __context__.SourceCodeLine = 841;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 841;
                Trace( "temp file pathNameExt: {0} \r\n", TMPFILEPATHNAME ) ; 
                }
            
            __context__.SourceCodeLine = 842;
            MakeString ( RVIFILEPATHNAME , "{0}{1}", APPPATH , G_SRVIFILENAME__DOLLAR__ ) ; 
            __context__.SourceCodeLine = 844;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 844;
                Trace( "rvi file pathNameExt: {0} \r\n", RVIFILEPATHNAME ) ; 
                }
            
            __context__.SourceCodeLine = 846;
            TMPFILEFOUND = (short) ( FindFirstShared( TMPFILEPATHNAME , ref TMPFILEINFO ) ) ; 
            __context__.SourceCodeLine = 847;
            FILEOPERR = (short) ( FindClose() ) ; 
            __context__.SourceCodeLine = 848;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPERR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 850;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 850;
                    Trace( "Find Close Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 851;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 852;
                FILEOPERR = (short) ( FindClose() ) ; 
                __context__.SourceCodeLine = 848;
                } 
            
            __context__.SourceCodeLine = 855;
            RVIFILEFOUND = (short) ( FindFirstShared( RVIFILEPATHNAME , ref RVIFILEINFO ) ) ; 
            __context__.SourceCodeLine = 856;
            FILEOPERR = (short) ( FindClose() ) ; 
            __context__.SourceCodeLine = 857;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPERR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 859;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 859;
                    Trace( "Find Close Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 860;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 861;
                FILEOPERR = (short) ( FindClose() ) ; 
                __context__.SourceCodeLine = 857;
                } 
            
            __context__.SourceCodeLine = 864;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 864;
                Trace( "tmpFileFound = {0:d}\r\n", (short)TMPFILEFOUND) ; 
                }
            
            __context__.SourceCodeLine = 865;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (TMPFILEFOUND == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 867;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RVIFILEFOUND == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 869;
                    TMPFILEHANDLE = (short) ( FileOpenShared( TMPFILEPATHNAME ,(ushort) (0 | 16384) ) ) ; 
                    __context__.SourceCodeLine = 870;
                    RVIFILEHANDLE = (short) ( FileOpenShared( RVIFILEPATHNAME ,(ushort) (0 | 16384) ) ) ; 
                    __context__.SourceCodeLine = 872;
                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 872;
                        Trace( "tmpFileOpened = {0:d}\r\n", (short)TMPFILEHANDLE) ; 
                        }
                    
                    __context__.SourceCodeLine = 873;
                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 873;
                        Trace( "rviFileOpened = {0:d}\r\n", (short)RVIFILEHANDLE) ; 
                        }
                    
                    __context__.SourceCodeLine = 875;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( TMPFILEHANDLE >= 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( RVIFILEHANDLE >= 0 ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 877;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( TMPFILEINFO.lSize > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( RVIFILEINFO.lSize > 0 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 879;
                            MATCHING = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 880;
                            MATCH = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 881;
                            while ( Functions.TestForTrue  ( ( MATCHING)  ) ) 
                                { 
                                __context__.SourceCodeLine = 883;
                                TMPREAD = (ushort) ( FileRead( (short)( TMPFILEHANDLE ) , TMPBUFFER , (ushort)( 1000 ) ) ) ; 
                                __context__.SourceCodeLine = 884;
                                RVIREAD = (ushort) ( FileRead( (short)( RVIFILEHANDLE ) , RVIBUFFER , (ushort)( 1000 ) ) ) ; 
                                __context__.SourceCodeLine = 885;
                                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                    {
                                    __context__.SourceCodeLine = 885;
                                    Trace( "tmp read: {0:d} \r\n rvi read: {1:d} \r\n", (short)TMPREAD, (short)RVIREAD) ; 
                                    }
                                
                                __context__.SourceCodeLine = 890;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( TMPREAD < 0 ) ) || Functions.TestForTrue ( Functions.BoolToInt ( RVIREAD < 0 ) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (TMPREAD != RVIREAD) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (TMPBUFFER != RVIBUFFER) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 892;
                                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                        {
                                        __context__.SourceCodeLine = 892;
                                        Trace( "not matched \r\n") ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 893;
                                    MATCHING = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 894;
                                    break ; 
                                    } 
                                
                                __context__.SourceCodeLine = 897;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( FileEOF( (short)( TMPFILEHANDLE ) ) ) && Functions.TestForTrue ( FileEOF( (short)( RVIFILEHANDLE ) ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 899;
                                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                        {
                                        __context__.SourceCodeLine = 899;
                                        Trace( "matched \r\n") ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 900;
                                    MATCH = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 901;
                                    MATCHING = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 902;
                                    break ; 
                                    } 
                                
                                __context__.SourceCodeLine = 881;
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 907;
                        if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 907;
                            Trace( "one or both file wont open for compare \r\n") ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 909;
                    TMPFILECLOSEERROR = (short) ( FileClose( (short)( TMPFILEHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 910;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (TMPFILECLOSEERROR != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 912;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 912;
                            Trace( "tmp Close Error Retrying\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 913;
                        Functions.Delay (  (int) ( 100 ) ) ; 
                        __context__.SourceCodeLine = 914;
                        TMPFILECLOSEERROR = (short) ( FileClose( (short)( TMPFILEHANDLE ) ) ) ; 
                        __context__.SourceCodeLine = 910;
                        } 
                    
                    __context__.SourceCodeLine = 917;
                    RVIFILECLOSEERROR = (short) ( FileClose( (short)( RVIFILEHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 918;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt (RVIFILECLOSEERROR != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 920;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 920;
                            Trace( "tmp Close Error Retrying\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 921;
                        Functions.Delay (  (int) ( 100 ) ) ; 
                        __context__.SourceCodeLine = 922;
                        TMPFILECLOSEERROR = (short) ( FileClose( (short)( RVIFILEHANDLE ) ) ) ; 
                        __context__.SourceCodeLine = 918;
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 925;
                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 925;
                        Trace( "app rvi file missing sometime between startup read and update compare: {0}\r\n", RVIFILEPATHNAME ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 927;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 927;
                    Trace( "no tmp file do whatever this program does: {0}\r\n", TMPFILEPATHNAME ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 929;
            SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
            __context__.SourceCodeLine = 930;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt (SNENDFILEIOERROR != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 932;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 932;
                    Trace( "End File Operations Error Retrying\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 933;
                Functions.Delay (  (int) ( 100 ) ) ; 
                __context__.SourceCodeLine = 934;
                SNENDFILEIOERROR = (short) ( EndFileOperations() ) ; 
                __context__.SourceCodeLine = 930;
                } 
            
            __context__.SourceCodeLine = 937;
            return (ushort)( MATCH) ; 
            
            }
            
        private void COPYANDDELETEFILES (  SplusExecutionContext __context__, ushort UNUSED ) 
            { 
            CrestronString SOURCEPATHNAMEEXT;
            SOURCEPATHNAMEEXT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            CrestronString DESTPATHNAMEEXT;
            DESTPATHNAMEEXT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 945;
            if ( Functions.TestForTrue  ( ( Functions.Not( BADSTORAGEPATH ))  ) ) 
                { 
                __context__.SourceCodeLine = 947;
                MakeString ( SOURCEPATHNAMEEXT , "{0}{1}{2}{3:d}", FILESTORAGELOCATION , G_SRVIFILENAME__DOLLAR__ , "_tmp" , (short)GetProgramNumber()) ; 
                __context__.SourceCodeLine = 949;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 949;
                    Trace( "sourcePathNameExt: {0}\r\n", SOURCEPATHNAMEEXT ) ; 
                    }
                
                __context__.SourceCodeLine = 950;
                MakeString ( DESTPATHNAMEEXT , "{0}{1}{2}{3:d}", G_SRVIFILELOCATION__DOLLAR__ , G_SRVIFILENAME__DOLLAR__ , "_app" , (short)GetProgramNumber()) ; 
                __context__.SourceCodeLine = 951;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 951;
                    Trace( "destPathNameExt: {0}\r\n", DESTPATHNAMEEXT ) ; 
                    }
                
                __context__.SourceCodeLine = 952;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.FileExists( SOURCEPATHNAMEEXT .ToString() ) != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 954;
                    if ( Functions.TestForTrue  ( ( Functions.Not( RVIMATCH( __context__ ) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 956;
                        MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Replacing RVI File with update...") ; 
                        __context__.SourceCodeLine = 957;
                        if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 957;
                            Trace( "Replacing rvi File with update...\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 959;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.CopyFile( SOURCEPATHNAMEEXT .ToString() , DESTPATHNAMEEXT .ToString() , (short)( 1 ) ) == 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 961;
                            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 961;
                                Trace( "rvi copied to app dir\r\n") ; 
                                }
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 965;
                            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 965;
                                Trace( "didnt copy rvi to app dir\r\n") ; 
                                }
                            
                            } 
                        
                        __context__.SourceCodeLine = 968;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.RenameFile( DESTPATHNAMEEXT .ToString() , G_SRVIFILENAME__DOLLAR__ .ToString() , (short)( 1 ) ) == 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 970;
                            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 970;
                                Trace( "rvi renamed in app dir\r\n") ; 
                                }
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 974;
                            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 974;
                                Trace( "didnt rename rvi in app dir\r\n") ; 
                                }
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 977;
                        if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 977;
                            Trace( "files match no update\r\n") ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 979;
                    MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Creating backup RVI File...") ; 
                    __context__.SourceCodeLine = 981;
                    MakeString ( DESTPATHNAMEEXT , "{0}{1}{2:d}", G_SRVIFILENAME__DOLLAR__ , "_app" , (short)GetProgramNumber()) ; 
                    __context__.SourceCodeLine = 982;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.RenameFile( SOURCEPATHNAMEEXT .ToString() , DESTPATHNAMEEXT .ToString() , (short)( 1 ) ) == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 984;
                        if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 984;
                            Trace( "converted working to backup rvi\r\n") ; 
                            }
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 988;
                        if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 988;
                            Trace( "cannot convert working to backup rvi\r\n") ; 
                            }
                        
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 992;
                    GenerateUserError ( "Fusion GUID Updater: working rvi file disappeared between the time it was created and it was to be copied to the app folder. File can be restored on restart if backup exsists. {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 995;
                GenerateUserError ( "Fusion GUID Updater: Cannot continue processing with an invalid storage location. {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                }
            
            
            }
            
        private void CLEAR_OUTPUTS (  SplusExecutionContext __context__ ) 
            { 
            ushort NLOOPI = 0;
            
            
            __context__.SourceCodeLine = 1002;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Clearing Current Data...") ; 
            __context__.SourceCodeLine = 1004;
            if ( Functions.TestForTrue  ( ( Functions.Not( G_NMASTERGUIDOVERRIDE ))  ) ) 
                { 
                __context__.SourceCodeLine = 1006;
                MakeString ( MASTER_GUID_PREFIX_TXT__DOLLAR__ , "") ; 
                } 
            
            __context__.SourceCodeLine = 1008;
            MakeString ( RVI_FILE_NAME_TXT__DOLLAR__ , "") ; 
            __context__.SourceCodeLine = 1009;
            MakeString ( RVI_FILE_FULL_PATH_TXT__DOLLAR__ , "") ; 
            __context__.SourceCodeLine = 1011;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)60; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( NLOOPI  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (NLOOPI  >= __FN_FORSTART_VAL__1) && (NLOOPI  <= __FN_FOREND_VAL__1) ) : ( (NLOOPI  <= __FN_FORSTART_VAL__1) && (NLOOPI  >= __FN_FOREND_VAL__1) ) ; NLOOPI  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 1018;
                G_NGUIDCOUNT [ NLOOPI] = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 1011;
                } 
            
            
            }
            
        private void UPDATEGUIDCOUNTS (  SplusExecutionContext __context__ ) 
            { 
            ushort NLOOPJ = 0;
            
            
            __context__.SourceCodeLine = 1026;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Updating GUID Counts...") ; 
            __context__.SourceCodeLine = 1028;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)60; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( NLOOPJ  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (NLOOPJ  >= __FN_FORSTART_VAL__1) && (NLOOPJ  <= __FN_FOREND_VAL__1) ) : ( (NLOOPJ  <= __FN_FORSTART_VAL__1) && (NLOOPJ  >= __FN_FOREND_VAL__1) ) ; NLOOPJ  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 1028;
                } 
            
            
            }
            
        private void UPDATE_GUIDS (  SplusExecutionContext __context__ ) 
            { 
            CrestronString SRVIFILENAME__DOLLAR__;
            SRVIFILENAME__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 1041;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1041;
                Trace( "Inside if(!Update_GUID_Finished || g_nInitialRun)\r\n") ; 
                }
            
            __context__.SourceCodeLine = 1042;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "GUID Update Started...") ; 
            __context__.SourceCodeLine = 1043;
            CLEAR_OUTPUTS (  __context__  ) ; 
            __context__.SourceCodeLine = 1044;
            G_NCONSOLESTEP = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1045;
            G_NTSIDUPDATEBUSY = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1046;
            MakeString ( CONSOLE_TX__DOLLAR__ , "{0}", "ver\r\n" ) ; 
            __context__.SourceCodeLine = 1047;
            while ( Functions.TestForTrue  ( ( G_NTSIDUPDATEBUSY)  ) ) 
                { 
                __context__.SourceCodeLine = 1049;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1049;
                    Trace( "Inside while(g_nTSIDUpdateBusy)\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 1050;
                Functions.ProcessLogic ( ) ; 
                __context__.SourceCodeLine = 1051;
                Functions.Delay (  (int) ( 200 ) ) ; 
                __context__.SourceCodeLine = 1047;
                } 
            
            __context__.SourceCodeLine = 1053;
            G_NFUSIONDATASTARTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 1054;
            G_NFUSIONSYMBOLDATASTARTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 1055;
            G_NFUSIONSYMBOLCOUNT = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1056;
            Functions.ClearBuffer ( G_SRVITEMP__DOLLAR__ ) ; 
            __context__.SourceCodeLine = 1057;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1057;
                Trace( "g_sRVITemp$ = {0}\r\n", G_SRVITEMP__DOLLAR__ ) ; 
                }
            
            __context__.SourceCodeLine = 1058;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1058;
                Trace( "len(g_sRVITemp$) = {0:d}\r\n", (short)Functions.Length( G_SRVITEMP__DOLLAR__ )) ; 
                }
            
            __context__.SourceCodeLine = 1060;
            MakeString ( SRVIFILENAME__DOLLAR__ , "{0}", G_SRVIFILENAME__DOLLAR__ ) ; 
            __context__.SourceCodeLine = 1062;
            if ( Functions.TestForTrue  ( ( Functions.Find( "XXXXXXXX" , G_SGUIDPREFIX__DOLLAR__[ G_NFUSIONSYMBOLCOUNT ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1064;
                MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "TSID Error...") ; 
                __context__.SourceCodeLine = 1065;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1065;
                    Trace( "TSID Error\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 1066;
                Functions.Delay (  (int) ( 500 ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1070;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SRVIFILENAME__DOLLAR__ != "FileFoundError"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1072;
                    MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "RVI File Found Processing File...") ; 
                    __context__.SourceCodeLine = 1073;
                    MakeString ( G_SRVIFULLFILEPATH__DOLLAR__ , "{0}{1}", G_SRVIFILELOCATION__DOLLAR__ , SRVIFILENAME__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1074;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1074;
                        Trace( "Inside if(sRVIFileName$ != csRVIFileSearchError)\r\ng_sRVIFullFilePath$ = {0}\r\n", G_SRVIFULLFILEPATH__DOLLAR__ ) ; 
                        }
                    
                    __context__.SourceCodeLine = 1075;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_NDEBUG == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1077;
                        
                            {
                            int __SPLS_TMPVAR__SWTCH_2__ = ((int)Functions.GetSeries());
                            
                                { 
                                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 2) ) ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1081;
                                    PROCESSRVIFILE (  __context__ , "\\SIMPL\\Fusion GUID Updater Simple Test Pro2 10-21-2013 rev3.rvi") ; 
                                    } 
                                
                                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 3) ) ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1085;
                                    PROCESSRVIFILE (  __context__ , "\\SIMPL\\App01\\ADG-PV-Room_230-140310.rvi") ; 
                                    } 
                                
                                } 
                                
                            }
                            
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 1091;
                        PROCESSRVIFILE (  __context__ , G_SRVIFULLFILEPATH__DOLLAR__) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1094;
                    UPDATEGUIDCOUNTS (  __context__  ) ; 
                    __context__.SourceCodeLine = 1095;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( G_NRVIFILECHANGED ) && Functions.TestForTrue ( Functions.BoolToInt (PREFIXGUIDS  .Value == 1) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1097;
                        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                            {
                            __context__.SourceCodeLine = 1097;
                            Trace( "Inside if(g_nRVIFileChanged && PrefixGUIDs = cnTrue)\r\n") ; 
                            }
                        
                        __context__.SourceCodeLine = 1098;
                        COPYANDDELETEFILES (  __context__ , (ushort)( 1 )) ; 
                        __context__.SourceCodeLine = 1099;
                        G_NRVIFILECHANGED = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1101;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PREFIXGUIDS  .Value == 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1103;
                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 1103;
                                Trace( "Inside ELSE if(PrefixGUIDs = cnTrue)\r\n") ; 
                                }
                            
                            __context__.SourceCodeLine = 1104;
                            COPYANDDELETEFILES (  __context__ , (ushort)( 2 )) ; 
                            __context__.SourceCodeLine = 1105;
                            G_NRVIFILECHANGED = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 1109;
                            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                                {
                                __context__.SourceCodeLine = 1109;
                                Trace( "Inside ELSE if(g_nRVIFileChanged && PrefixGUIDs = cnTrue)\r\n") ; 
                                }
                            
                            __context__.SourceCodeLine = 1110;
                            G_NRVIFILECHANGED = (ushort) ( 0 ) ; 
                            } 
                        
                        }
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 1115;
                    MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "RVI File Error...") ; 
                    __context__.SourceCodeLine = 1116;
                    Functions.Delay (  (int) ( 500 ) ) ; 
                    __context__.SourceCodeLine = 1117;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1117;
                        Trace( "FindRVIFile Error\r\n") ; 
                        }
                    
                    } 
                
                } 
            
            __context__.SourceCodeLine = 1120;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "GUID Update Complete!") ; 
            __context__.SourceCodeLine = 1122;
            Functions.Pulse ( 50, UPDATE_GUID_FINISHED ) ; 
            __context__.SourceCodeLine = 1123;
            Functions.Delay (  (int) ( 500 ) ) ; 
            __context__.SourceCodeLine = 1124;
            MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "") ; 
            
            }
            
        private void DELETELINGERINGTEMPFILE (  SplusExecutionContext __context__ ) 
            { 
            CrestronString TMPPATHNAMEEXT;
            TMPPATHNAMEEXT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            ushort PROGRAMNUMBER = 0;
            
            
            __context__.SourceCodeLine = 1150;
            PROGRAMNUMBER = (ushort) ( GetProgramNumber() ) ; 
            __context__.SourceCodeLine = 1151;
            MakeString ( TMPPATHNAMEEXT , "{0}{1}{2:d}", G_SRVIFILENAME__DOLLAR__ , "_tmp" , (short)GetProgramNumber()) ; 
            __context__.SourceCodeLine = 1154;
            MakeString ( TMPPATHNAMEEXT , "{0}{1}", FILESTORAGELOCATION , TMPPATHNAMEEXT ) ; 
            __context__.SourceCodeLine = 1155;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1155;
                Trace( "file to purge: {0}\r\n", TMPPATHNAMEEXT ) ; 
                }
            
            __context__.SourceCodeLine = 1157;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.FileExists( TMPPATHNAMEEXT .ToString() ) == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 1159;
                GenerateUserError ( "Fusion GUID Updater Helper: Found invalid working rvi file, marked for removal: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                __context__.SourceCodeLine = 1160;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.DeleteFile( TMPPATHNAMEEXT .ToString() ) == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 1161;
                    GenerateUserError ( "Fusion GUID Updater Helper: invalid working rvi file purged: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 1163;
                    GenerateUserError ( "Fusion GUID Updater Helper: invalid working rvi could not be purged, check for rvi file corruption: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1165;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1165;
                    Trace( "no file to purge\r\n") ; 
                    }
                
                }
            
            
            }
            
        private void RESTOREMISSINGRVI (  SplusExecutionContext __context__ ) 
            { 
            CrestronString FILENAME;
            FILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            CrestronString BACKUPFILEPATHNAME;
            BACKUPFILEPATHNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            CrestronString APPFILEPATHNAME;
            APPFILEPATHNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
            
            
            __context__.SourceCodeLine = 1178;
            FILENAME  .UpdateValue ( G_SRVIFILENAME__DOLLAR__  ) ; 
            __context__.SourceCodeLine = 1180;
            if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1180;
                Trace( "app path: {0}, fileName: {1}\r\n", G_SRVIFILELOCATION__DOLLAR__ , FILENAME ) ; 
                }
            
            __context__.SourceCodeLine = 1182;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILENAME == "FileFoundError"))  ) ) 
                { 
                __context__.SourceCodeLine = 1186;
                GenerateUserError ( "Fusion GUID Updater Helper, missing application .rvi file, attempting to restore: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                __context__.SourceCodeLine = 1188;
                MakeString ( BACKUPFILEPATHNAME , "{0}{1}{2:d}", "*.rvi" , "_app" , (short)GetProgramNumber()) ; 
                __context__.SourceCodeLine = 1189;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1189;
                    Trace( "back up search terms: {0}\r\n", BACKUPFILEPATHNAME ) ; 
                    }
                
                __context__.SourceCodeLine = 1191;
                FILENAME  .UpdateValue ( FILEOPS . GetFirstFileNameByExt (  FILESTORAGELOCATION  .ToString() ,  BACKUPFILEPATHNAME  .ToString() )  ) ; 
                __context__.SourceCodeLine = 1192;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1192;
                    Trace( "back up found: {0}\r\n", FILENAME ) ; 
                    }
                
                __context__.SourceCodeLine = 1193;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1195;
                    MakeString ( BACKUPFILEPATHNAME , "{0}{1}.rvi{2}{3:d}", FILESTORAGELOCATION , FILENAME , "_app" , (short)GetProgramNumber()) ; 
                    __context__.SourceCodeLine = 1196;
                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1196;
                        Trace( "back source: {0}\r\n", BACKUPFILEPATHNAME ) ; 
                        }
                    
                    __context__.SourceCodeLine = 1197;
                    MakeString ( APPFILEPATHNAME , "{0}{1}.rvi", G_SRVIFILELOCATION__DOLLAR__ , FILENAME ) ; 
                    __context__.SourceCodeLine = 1198;
                    if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1198;
                        Trace( "app destion: {0}\r\n", APPFILEPATHNAME ) ; 
                        }
                    
                    __context__.SourceCodeLine = 1199;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FILEOPS.CopyFile( BACKUPFILEPATHNAME .ToString() , APPFILEPATHNAME .ToString() , (short)( 0 ) ) == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1201;
                        SEEKRVIFILE (  __context__  ) ; 
                        __context__.SourceCodeLine = 1202;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (G_SRVIFILENAME__DOLLAR__ != "FileFoundError"))  ) ) 
                            {
                            __context__.SourceCodeLine = 1203;
                            GenerateUserError ( "Fusion GUID Updater Helper: missing application .rvi file restored and re-registered: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1205;
                            GenerateUserError ( "Fusion GUID Updater Helper: missing application .rvi file restored but then couldn't be re-registered: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1208;
                        GenerateUserError ( "Fusion GUID Updater Helper: missing application .rvi file restore failed: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1211;
                    GenerateUserError ( "Fusion GUID Updater Helper: restore fail, there is no backup to restore from: {0} at {1}\r\n", Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1215;
                if ( Functions.TestForTrue  ( ( DBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1215;
                    Trace( "rvi not missing\r\n") ; 
                    }
                
                }
            
            
            }
            
        object UPDATE_GUIDS_B_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 1223;
                if ( Functions.TestForTrue  ( ( INITIALIZED)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1225;
                    UPDATE_GUIDS (  __context__  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 1229;
                    UPDATEGUIDRECEIVED = (ushort) ( 1 ) ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object MASTER_OVERRIDE_GUID_PREFIX__DOLLAR___OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort NLOOP = 0;
            
            CrestronString STEMPNEWGUID__DOLLAR__;
            STEMPNEWGUID__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 1256;
            if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 1256;
                Trace( "***** Change Master_Override_GUID_Prefix$ *****\r\n") ; 
                }
            
            __context__.SourceCodeLine = 1257;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__ != ""))  ) ) 
                { 
                __context__.SourceCodeLine = 1259;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1259;
                    Trace( "***** Inside if(Master_Override_GUID_Prefix$ <> \u0022\u0022) *****\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 1260;
                G_NMASTERGUIDOVERRIDE = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 1261;
                Functions.SetArray (  ref G_NGUIDOVERRIDE , (ushort)1) ; 
                __context__.SourceCodeLine = 1262;
                MakeString ( G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ , "{0}", MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 1263;
                MakeString ( STEMPNEWGUID__DOLLAR__ , "{0}", UPDATEGUIDPREFIX (  __context__ , G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__, (ushort)( 1000 )) ) ; 
                __context__.SourceCodeLine = 1264;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)60; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( NLOOP  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (NLOOP  >= __FN_FORSTART_VAL__1) && (NLOOP  <= __FN_FOREND_VAL__1) ) : ( (NLOOP  <= __FN_FORSTART_VAL__1) && (NLOOP  >= __FN_FOREND_VAL__1) ) ; NLOOP  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 1266;
                    MakeString ( G_SGUIDPREFIX__DOLLAR__ [ NLOOP ] , "{0}", STEMPNEWGUID__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1264;
                    } 
                
                __context__.SourceCodeLine = 1269;
                MakeString ( G_SMASTERGUIDPREFIX__DOLLAR__ , "{0}", STEMPNEWGUID__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 1270;
                MakeString ( MASTER_GUID_PREFIX_TXT__DOLLAR__ , "{0}", STEMPNEWGUID__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 1271;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1271;
                    Trace( "sTempNewGUID$ = {0}\r\n", STEMPNEWGUID__DOLLAR__ ) ; 
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1275;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1275;
                    Trace( "***** Inside ELSE for if(Master_Override_GUID_Prefix$ <> \u0022\u0022) *****\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 1276;
                G_NMASTERGUIDOVERRIDE = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 1277;
                Functions.SetArray (  ref G_NGUIDOVERRIDE , (ushort)0) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object CONSOLE_RX__DOLLAR___OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort NTSIDSTARTPOS = 0;
        ushort NTSIDENDPOS = 0;
        ushort NTSIDCOUNT = 0;
        ushort NLOOP = 0;
        
        CrestronString SCONSOLESEARCHTEMP__DOLLAR__;
        CrestronString STEMPTSID__DOLLAR__;
        SCONSOLESEARCHTEMP__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        STEMPTSID__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        
        
        __context__.SourceCodeLine = 1307;
        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 1307;
            Trace( "***** Start Console_rx$ *****\r\n") ; 
            }
        
        __context__.SourceCodeLine = 1309;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( G_NCONSOLESTEP > 0 ) ) && Functions.TestForTrue ( Functions.Find( ">" , CONSOLE_RX__DOLLAR__ ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( G_NTSIDRETRYCOUNT <= 5 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1311;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "#" , CONSOLE_RX__DOLLAR__ ) ) && Functions.TestForTrue ( Functions.Find( "]" , CONSOLE_RX__DOLLAR__ ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (G_NCONSOLESTEP == 1) )) ) ) && Functions.TestForTrue ( Functions.Not( G_NMASTERGUIDOVERRIDE ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1313;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1313;
                    Trace( "***** Inside Console If for TSID *****\r\n") ; 
                    }
                
                __context__.SourceCodeLine = 1314;
                NTSIDSTARTPOS = (ushort) ( (Functions.Find( "#" , CONSOLE_RX__DOLLAR__ ) + 1) ) ; 
                __context__.SourceCodeLine = 1315;
                NTSIDENDPOS = (ushort) ( Functions.Find( "]" , CONSOLE_RX__DOLLAR__ ) ) ; 
                __context__.SourceCodeLine = 1316;
                NTSIDCOUNT = (ushort) ( (NTSIDENDPOS - NTSIDSTARTPOS) ) ; 
                __context__.SourceCodeLine = 1317;
                MakeString ( STEMPTSID__DOLLAR__ , "{0}", Functions.Lower ( Functions.Mid ( CONSOLE_RX__DOLLAR__ ,  (int) ( NTSIDSTARTPOS ) ,  (int) ( NTSIDCOUNT ) ) ) ) ; 
                __context__.SourceCodeLine = 1318;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1320;
                    Trace( "nTSIDStartPOS = {0:d}\r\n", (short)NTSIDSTARTPOS) ; 
                    __context__.SourceCodeLine = 1321;
                    Trace( "nTSIDEndPOS = {0:d}\r\n", (short)NTSIDENDPOS) ; 
                    __context__.SourceCodeLine = 1322;
                    Trace( "nTSIDCount = {0:d}\r\n", (short)NTSIDCOUNT) ; 
                    __context__.SourceCodeLine = 1323;
                    Trace( "sTempTSID$ = {0}\r\n", STEMPTSID__DOLLAR__ ) ; 
                    } 
                
                __context__.SourceCodeLine = 1325;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( STEMPTSID__DOLLAR__ ) < 8 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1327;
                    Functions.ClearBuffer ( CONSOLE_RX__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1328;
                    G_NTSIDRETRYCOUNT = (ushort) ( (G_NTSIDRETRYCOUNT + 1) ) ; 
                    __context__.SourceCodeLine = 1329;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1329;
                        Trace( "TSID Error, Retrying TSID, Retry Count: {0:d}\r\n", (short)G_NTSIDRETRYCOUNT) ; 
                        }
                    
                    __context__.SourceCodeLine = 1330;
                    MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "Invalid TSID Received, Retrying... Retry Count: {0:d}", (short)G_NTSIDRETRYCOUNT) ; 
                    __context__.SourceCodeLine = 1331;
                    G_NCONSOLESTEP = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1332;
                    MakeString ( CONSOLE_TX__DOLLAR__ , "{0}", "ver\r\n" ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 1337;
                    G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__  .UpdateValue ( UPDATEGUIDPREFIX (  __context__ , STEMPTSID__DOLLAR__, (ushort)( 1001 ))  ) ; 
                    __context__.SourceCodeLine = 1338;
                    MakeString ( MASTER_GUID_PREFIX_TXT__DOLLAR__ , "{0}", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1339;
                    MakeString ( G_SMASTERGUIDPREFIX__DOLLAR__ , "{0}", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1340;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1340;
                        Trace( "g_sGUIDMasterOverridePrefix$ = {0}\r\n", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                        }
                    
                    __context__.SourceCodeLine = 1341;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)60; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( NLOOP  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (NLOOP  >= __FN_FORSTART_VAL__1) && (NLOOP  <= __FN_FOREND_VAL__1) ) : ( (NLOOP  <= __FN_FORSTART_VAL__1) && (NLOOP  >= __FN_FOREND_VAL__1) ) ; NLOOP  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 1343;
                        if ( Functions.TestForTrue  ( ( Functions.Not( G_NGUIDOVERRIDE[ NLOOP ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1345;
                            MakeString ( G_SGUIDPREFIX__DOLLAR__ [ NLOOP ] , "{0}", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 1341;
                        } 
                    
                    __context__.SourceCodeLine = 1348;
                    Functions.ClearBuffer ( CONSOLE_RX__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1349;
                    G_NCONSOLESTEP = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1350;
                    G_NTSIDRETRYCOUNT = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1351;
                    G_NTSIDUPDATEBUSY = (ushort) ( 0 ) ; 
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1354;
                if ( Functions.TestForTrue  ( ( G_NMASTERGUIDOVERRIDE)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1356;
                    if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                        {
                        __context__.SourceCodeLine = 1356;
                        Trace( "Inside else if(g_nGUIDOverride)\r\n") ; 
                        }
                    
                    __context__.SourceCodeLine = 1357;
                    Functions.ClearBuffer ( CONSOLE_RX__DOLLAR__ ) ; 
                    __context__.SourceCodeLine = 1358;
                    G_NCONSOLESTEP = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1359;
                    G_NTSIDRETRYCOUNT = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1360;
                    G_NTSIDUPDATEBUSY = (ushort) ( 0 ) ; 
                    } 
                
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1363;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( G_NTSIDRETRYCOUNT > 5 ))  ) ) 
                { 
                __context__.SourceCodeLine = 1365;
                if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 1365;
                    Trace( "TSID Error, Max Retries Reached, Stopping Process") ; 
                    }
                
                __context__.SourceCodeLine = 1366;
                MakeString ( UPDATE_GUID_STATUS_TXT__DOLLAR__ , "TSID Error, Max Retries Reached, Stopping Process") ; 
                __context__.SourceCodeLine = 1367;
                MakeString ( G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ , "{0}", UPDATEGUIDPREFIX (  __context__ , "XXXXXXXX", (ushort)( 1001 )) ) ; 
                __context__.SourceCodeLine = 1368;
                MakeString ( MASTER_GUID_PREFIX_TXT__DOLLAR__ , "{0}", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                __context__.SourceCodeLine = 1369;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)60; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( NLOOP  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (NLOOP  >= __FN_FORSTART_VAL__2) && (NLOOP  <= __FN_FOREND_VAL__2) ) : ( (NLOOP  <= __FN_FORSTART_VAL__2) && (NLOOP  >= __FN_FOREND_VAL__2) ) ; NLOOP  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 1371;
                    if ( Functions.TestForTrue  ( ( Functions.Not( G_NGUIDOVERRIDE[ NLOOP ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1373;
                        MakeString ( G_SGUIDPREFIX__DOLLAR__ [ NLOOP ] , "{0}", G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__ ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1369;
                    } 
                
                __context__.SourceCodeLine = 1376;
                G_NTSIDUPDATEBUSY = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1378;
                if ( Functions.TestForTrue  ( ( Functions.Find( ">" , CONSOLE_RX__DOLLAR__ ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1380;
                    Functions.ClearBuffer ( CONSOLE_RX__DOLLAR__ ) ; 
                    } 
                
                }
            
            }
        
        __context__.SourceCodeLine = 1383;
        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 1383;
            Trace( "***** End Console_rx$ *****\r\n") ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort NCOUNT = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1413;
        G_NDEBUG = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1414;
        G_NDEBUGRVIFILE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1415;
        DBUG = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1416;
        BADSTORAGEPATH = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1417;
        INITIALIZED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1418;
        UPDATEGUIDRECEIVED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1420;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 1422;
        G_NDEBUG = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1423;
        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 1423;
            Trace( "***** Start Function Main *****\r\n") ; 
            }
        
        __context__.SourceCodeLine = 1425;
        G_NINITIALRUN = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 1426;
        G_NCONSOLESTEP = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1427;
        G_NMASTERGUIDOVERRIDE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1428;
        Functions.SetArray (  ref G_NGUIDOVERRIDE , (ushort)0) ; 
        __context__.SourceCodeLine = 1429;
        G_NINSTANCEIDFOUND = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1430;
        G_NTSIDRETRYCOUNT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1431;
        G_NTSIDUPDATEBUSY = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1432;
        G_NRVIFILECHANGED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1433;
        G_NROOMNAMEOVERRIDEALLOWED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1435;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)60; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( NCOUNT  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (NCOUNT  >= __FN_FORSTART_VAL__1) && (NCOUNT  <= __FN_FOREND_VAL__1) ) : ( (NCOUNT  <= __FN_FORSTART_VAL__1) && (NCOUNT  >= __FN_FOREND_VAL__1) ) ; NCOUNT  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1437;
            Functions.ClearBuffer ( G_SOVERRIDEROOMNAME__DOLLAR__ [ NCOUNT ] ) ; 
            __context__.SourceCodeLine = 1435;
            } 
        
        __context__.SourceCodeLine = 1439;
        G_NROOMNAMEOVERRIDEALLOWED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 1442;
        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 1442;
            Trace( "***** Function Main Complete Variable Init *****\r\n") ; 
            }
        
        __context__.SourceCodeLine = 1446;
        if ( Functions.TestForTrue  ( ( PROCESSORMODE  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 1448;
            Functions.Delay (  (int) ( 12000 ) ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1452;
            Functions.Delay (  (int) ( 2000 ) ) ; 
            } 
        
        __context__.SourceCodeLine = 1455;
        ACQUIREAPPPATH (  __context__  ) ; 
        __context__.SourceCodeLine = 1456;
        SEEKRVIFILE (  __context__  ) ; 
        __context__.SourceCodeLine = 1457;
        FILESTORAGELOCATION  .UpdateValue ( "\\User\\Fusion\\"  ) ; 
        __context__.SourceCodeLine = 1458;
        if ( Functions.TestForTrue  ( ( FILEOPS.CheckOrCreateDirectory( FILESTORAGELOCATION .ToString() ))  ) ) 
            { 
            __context__.SourceCodeLine = 1460;
            DELETELINGERINGTEMPFILE (  __context__  ) ; 
            __context__.SourceCodeLine = 1461;
            RESTOREMISSINGRVI (  __context__  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1464;
            GenerateUserError ( "Fusion GUID Updater Helper: could not find/create working directory: <{0}>. {1} at {2}\r\n", FILESTORAGELOCATION , Functions.Date (  (int) ( 1 ) ) , Functions.Time ( ) ) ; 
            }
        
        __context__.SourceCodeLine = 1465;
        Functions.ClearBuffer ( G_SRVINEWFILETEMP__DOLLAR__ ) ; 
        __context__.SourceCodeLine = 1467;
        MakeString ( 1 , "BROAD OFF\r") ; 
        __context__.SourceCodeLine = 1470;
        INITIALIZED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 1471;
        if ( Functions.TestForTrue  ( ( UPDATEGUIDRECEIVED)  ) ) 
            { 
            __context__.SourceCodeLine = 1473;
            UPDATE_GUIDS (  __context__  ) ; 
            } 
        
        __context__.SourceCodeLine = 1476;
        if ( Functions.TestForTrue  ( ( G_NDEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 1476;
            Trace( "***** End Function Main *****\r\n") ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    G_NGUIDOVERRIDE  = new ushort[ 61 ];
    G_NROOMNAMEUPDATED  = new ushort[ 61 ];
    G_NGUIDCOUNT  = new ushort[ 61 ];
    G_SMASTERGUIDPREFIX__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 12, this );
    G_SGUIDMASTEROVERRIDEPREFIX__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 12, this );
    G_SRVITEMP__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5500, this );
    G_SRVINEWFILETEMP__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5500, this );
    G_SRVIFULLFILEPATH__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    G_SRVINEWFULLFILEPATH__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    G_SRVIFILENAME__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    G_SRVIFILELOCATION__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
    FILESTORAGELOCATION  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
    G_SGUIDPREFIX__DOLLAR__  = new CrestronString[ 61 ];
    for( uint i = 0; i < 61; i++ )
        G_SGUIDPREFIX__DOLLAR__ [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 12, this );
    G_SOVERRIDEROOMNAME__DOLLAR__  = new CrestronString[ 61 ];
    for( uint i = 0; i < 61; i++ )
        G_SOVERRIDEROOMNAME__DOLLAR__ [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    TCPCONSOLE  = new SplusTcpClient ( 1000, this );
    
    UPDATE_GUIDS_B = new Crestron.Logos.SplusObjects.DigitalInput( UPDATE_GUIDS_B__DigitalInput__, this );
    m_DigitalInputList.Add( UPDATE_GUIDS_B__DigitalInput__, UPDATE_GUIDS_B );
    
    UPDATE_GUID_FINISHED = new Crestron.Logos.SplusObjects.DigitalOutput( UPDATE_GUID_FINISHED__DigitalOutput__, this );
    m_DigitalOutputList.Add( UPDATE_GUID_FINISHED__DigitalOutput__, UPDATE_GUID_FINISHED );
    
    MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( MASTER_OVERRIDE_GUID_PREFIX__DOLLAR____AnalogSerialInput__, 8, this );
    m_StringInputList.Add( MASTER_OVERRIDE_GUID_PREFIX__DOLLAR____AnalogSerialInput__, MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__ );
    
    UPDATE_GUID_STATUS_TXT__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( UPDATE_GUID_STATUS_TXT__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( UPDATE_GUID_STATUS_TXT__DOLLAR____AnalogSerialOutput__, UPDATE_GUID_STATUS_TXT__DOLLAR__ );
    
    MASTER_GUID_PREFIX_TXT__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( MASTER_GUID_PREFIX_TXT__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( MASTER_GUID_PREFIX_TXT__DOLLAR____AnalogSerialOutput__, MASTER_GUID_PREFIX_TXT__DOLLAR__ );
    
    RVI_FILE_NAME_TXT__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( RVI_FILE_NAME_TXT__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( RVI_FILE_NAME_TXT__DOLLAR____AnalogSerialOutput__, RVI_FILE_NAME_TXT__DOLLAR__ );
    
    RVI_FILE_FULL_PATH_TXT__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( RVI_FILE_FULL_PATH_TXT__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( RVI_FILE_FULL_PATH_TXT__DOLLAR____AnalogSerialOutput__, RVI_FILE_FULL_PATH_TXT__DOLLAR__ );
    
    CONSOLE_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( CONSOLE_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( CONSOLE_TX__DOLLAR____AnalogSerialOutput__, CONSOLE_TX__DOLLAR__ );
    
    CONSOLE_STATUS_TXT__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( CONSOLE_STATUS_TXT__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( CONSOLE_STATUS_TXT__DOLLAR____AnalogSerialOutput__, CONSOLE_STATUS_TXT__DOLLAR__ );
    
    ROOMNAME__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( ROOMNAME__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMNAME__DOLLAR____AnalogSerialOutput__, ROOMNAME__DOLLAR__ );
    
    ROOMGUID__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( ROOMGUID__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMGUID__DOLLAR____AnalogSerialOutput__, ROOMGUID__DOLLAR__ );
    
    SYMBOL_SLOT_GUIDS_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( SYMBOL_SLOT_GUIDS_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( SYMBOL_SLOT_GUIDS_TX__DOLLAR____AnalogSerialOutput__, SYMBOL_SLOT_GUIDS_TX__DOLLAR__ );
    
    CONSOLE_RX__DOLLAR__ = new Crestron.Logos.SplusObjects.BufferInput( CONSOLE_RX__DOLLAR____AnalogSerialInput__, 1000, this );
    m_StringInputList.Add( CONSOLE_RX__DOLLAR____AnalogSerialInput__, CONSOLE_RX__DOLLAR__ );
    
    PROCESSORMODE = new UShortParameter( PROCESSORMODE__Parameter__, this );
    m_ParameterList.Add( PROCESSORMODE__Parameter__, PROCESSORMODE );
    
    PREFIXGUIDS = new UShortParameter( PREFIXGUIDS__Parameter__, this );
    m_ParameterList.Add( PREFIXGUIDS__Parameter__, PREFIXGUIDS );
    
    THREESERIESAPPENDSLOTNUMBER = new UShortParameter( THREESERIESAPPENDSLOTNUMBER__Parameter__, this );
    m_ParameterList.Add( THREESERIESAPPENDSLOTNUMBER__Parameter__, THREESERIESAPPENDSLOTNUMBER );
    
    FILESTORAGELOCATION__DOLLAR__ = new StringParameter( FILESTORAGELOCATION__DOLLAR____Parameter__, this );
    m_ParameterList.Add( FILESTORAGELOCATION__DOLLAR____Parameter__, FILESTORAGELOCATION__DOLLAR__ );
    
    
    UPDATE_GUIDS_B.OnDigitalPush.Add( new InputChangeHandlerWrapper( UPDATE_GUIDS_B_OnPush_0, true ) );
    MASTER_OVERRIDE_GUID_PREFIX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( MASTER_OVERRIDE_GUID_PREFIX__DOLLAR___OnChange_1, false ) );
    CONSOLE_RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( CONSOLE_RX__DOLLAR___OnChange_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    FILEOPS  = new SSFileOps.SimplSharpFileOps();
    
    
}

public CrestronModuleClass_FUSION_SSI_GUID_UPDATER_HELPER_V1_0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint UPDATE_GUIDS_B__DigitalInput__ = 0;
const uint MASTER_OVERRIDE_GUID_PREFIX__DOLLAR____AnalogSerialInput__ = 0;
const uint CONSOLE_RX__DOLLAR____AnalogSerialInput__ = 1;
const uint UPDATE_GUID_FINISHED__DigitalOutput__ = 0;
const uint UPDATE_GUID_STATUS_TXT__DOLLAR____AnalogSerialOutput__ = 0;
const uint MASTER_GUID_PREFIX_TXT__DOLLAR____AnalogSerialOutput__ = 1;
const uint RVI_FILE_NAME_TXT__DOLLAR____AnalogSerialOutput__ = 2;
const uint RVI_FILE_FULL_PATH_TXT__DOLLAR____AnalogSerialOutput__ = 3;
const uint CONSOLE_TX__DOLLAR____AnalogSerialOutput__ = 4;
const uint CONSOLE_STATUS_TXT__DOLLAR____AnalogSerialOutput__ = 5;
const uint ROOMNAME__DOLLAR____AnalogSerialOutput__ = 6;
const uint ROOMGUID__DOLLAR____AnalogSerialOutput__ = 7;
const uint SYMBOL_SLOT_GUIDS_TX__DOLLAR____AnalogSerialOutput__ = 8;
const uint PROCESSORMODE__Parameter__ = 10;
const uint PREFIXGUIDS__Parameter__ = 11;
const uint THREESERIESAPPENDSLOTNUMBER__Parameter__ = 12;
const uint FILESTORAGELOCATION__DOLLAR____Parameter__ = 13;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
